/*NTCAP Copyright (C) 2021-2022  Georgij Krajnyukov <nrk63@yandex.ru>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package ru.ntcap.core.initializers;

import java.util.function.IntBinaryOperator;

/**
 * Cellular plane initializer that fills specified shape.
 *
 * @author Georgij Krajnyukov
 * @version 0.1.0
 */
public abstract class ShapeInitializer implements IntBinaryOperator {
    /**
     * Returns state of the cell depending on is it contains in the shape or not.
     * @param x x-coordinate of the cell
     * @param y y-coordinate of the cell
     * @return state of the cell
     *
     * @implSpec
     * Uses {@link #contains(int, int)} to check if shape contains the cell.
     * If it does, returns result of {@link #initializeInner(int, int)}.
     * Otherwise, returns result of {@link #initializeOuter(int, int)}.
     */
    @Override
    public int applyAsInt(int x, int y) {
        if (contains(x, y))
            return initializeInner(x, y);
        return initializeOuter(x, y);
    }

    /**
     * Returns state of the cell that shape do contain.
     * @param x x-coordinate of the cell
     * @param y y-coordinate of the cell
     * @return state of the cell
     */
    protected abstract int initializeInner(int x, int y);

    /**
     * Returns state of the cell that shape do not contain.
     * @param x x-coordinate of the cell
     * @param y y-coordinate of the cell
     * @return state of the cell
     */
    protected int initializeOuter(int x, int y) {
        return 0;
    }

    /**
     * Checks if the shape contains parameter cell.
     * @param x x-coordinate of the cell
     * @param y y-coordinate of the cell
     * @return {@code true} if shape contains the cell; {@code false} otherwise
     */
    protected abstract boolean contains(int x, int y);
}
