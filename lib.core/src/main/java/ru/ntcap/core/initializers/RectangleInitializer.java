/*NTCAP Copyright (C) 2021-2022  Georgij Krajnyukov <nrk63@yandex.ru>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package ru.ntcap.core.initializers;

/**
 * {@code ShapeInitializer} that fills specified rectangle.
 * @see ShapeInitializer
 *
 * @author Georgij Krajnyukov
 * @version 0.1.0
 */
public abstract class RectangleInitializer extends ShapeInitializer {
    /**
     * Constructs new instance that fills the rectangle area specified by its upper left
     * corner position, width and height.
     * @param x rectangle upper left corner x-coordinate
     * @param y rectangle upper left corner y-coordinate
     * @param width rectangle width
     * @param height rectangle height
     */
    public RectangleInitializer(int x, int y, int width, int height) {
        upperLeftX= x;
        upperLeftY = y;
        bottomRightX = x + width;
        bottomRightY = y + height;
    }

    /**
     * Constructs new instance that fills square area specified by its upper left
     * corner position and side length.
     * @param x square upper left corner x-coordinate
     * @param y square upper left corner y-coordinate
     * @param length square's side length
     */
    public RectangleInitializer(int x, int y, int length) {
        this(x, y, length, length);
    }

    @Override
    protected boolean contains(int x, int y) {
        return x >= upperLeftX && x <= bottomRightX
               && y >= upperLeftY && y <= bottomRightY;
    }

    private final int upperLeftX;
    private final int upperLeftY;
    private final int bottomRightX;
    private final int bottomRightY;
}
