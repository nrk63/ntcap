/*NTCAP Copyright (C) 2021-2022  Georgij Krajnyukov <nrk63@yandex.ru>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package ru.ntcap.core.initializers;

/**
 * {@code ShapeInitializer} that fills specified circle.
 * @see ShapeInitializer
 *
 * @author Georgij Krajnyukov
 * @version 0.1.0
 */
public abstract class CircleInitializer extends ShapeInitializer {
    /**
     * Creates new instance that fills circle area specified by its center position
     * and radius.
     * @param centerX circle center x-coordinate
     * @param centerY circle center y-coordinate
     * @param radius circle radius
     */
    public CircleInitializer(int centerX, int centerY, long radius) {
        this.centerX = centerX;
        this.centerY = centerY;
        radiusSquared = radius * radius;
    }

    /**
     * Checks if circle contains specified cell by evaluating trivial inequality:
     * {@code (x - cx)^2 + (y - cy)^2 <= r^2}
     * @param x x-coordinate of the cell
     * @param y y-coordinate of the cell
     * @return {@code true} if circle contains the cell; {@code false} otherwise
     */
    protected boolean contains(int x, int y) {
        int shiftedX = x - centerX;
        int shiftedY = y - centerY;
        return (long)shiftedX*shiftedX + (long)shiftedY*shiftedY <= radiusSquared;
    }

    private final int centerX;
    private final int centerY;

    /**
     * squared radius of specified circle
     */
    private final long radiusSquared;
}
