/*NTCAP Copyright (C) 2021-2022  Georgij Krajnyukov <nrk63@yandex.ru>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package ru.ntcap.core.initializers;

import ru.ntcap.core.CellularAutomaton;

import java.util.function.IntBinaryOperator;

/**
 * Adapter for cellular plane initializers that emulates torus surface.
 * @see CellularAutomaton#initPlane(int, IntBinaryOperator)
 *
 * @author Georgij Krajnyukov
 * @version 0.1.0
 */
public class TorusInitializerAdapter implements IntBinaryOperator {
    public TorusInitializerAdapter(int width, int height, IntBinaryOperator initializer) {
        this.width = width;
        this.height = height;
        this.initializer = initializer;
    }

    /**
     * Returns the state of specified cell. Explores torus surface by trying to receive
     * non-zero state of
     * {@code (x +- width; y), (x, y +- height), (x +- width, y +- height)} points.
     * @param x x-coordinate of the cell
     * @param y y-coordinate of the cell
     * @return state of the cell
     */
    @Override
    public int applyAsInt(int x, int y) {
        for (int torusX = x - width; torusX <= x + width; torusX += width) {
            for (int torusY = y - height; torusY <= y + height; torusY += height) {
                int state = initializer.applyAsInt(torusX, torusY);
                if (state != 0)
                    return state;
            }
        }
        return 0;
    }

    private final int width;
    private final int height;
    private final IntBinaryOperator initializer;
}
