package ru.ntcap.core.neighbourhood;

import java.util.function.IntPredicate;

/**
 * This class represents 2x2 block Margolus neighbourhood which consists of center itself
 * and cells oriented <i>clockwise</i>, diagonal (<i>opposite</i>) and <i>counterclockwise</i>
 * relative to the center.
 * @author Georgij Krajnyukov
 * @version 0.1.0
 */
public class MargolusNeighborhood {
    public MargolusNeighborhood(int center, int clockwise, int opposite, int counterClockwise) {
        this.center = center;
        this.clockwise = clockwise;
        this.opposite = opposite;
        this.counterClockwise = counterClockwise;
    }

    /**
     * Counts neighbours, except for center, that satisfies the predicate.
     * @param predicate predicate which returns true for cells that should be counted
     * @return number of neighbours satisfying the predicate
     */
    public int countIf(IntPredicate predicate) {
        int result = 0;
        if (predicate.test(clockwise))
            ++result;
        if (predicate.test(opposite))
            ++result;
        if (predicate.test(counterClockwise))
            ++result;
        return result;
    }

    public int getCenter() {
        return center;
    }

    public int getClockwise() {
        return clockwise;
    }

    public int getOpposite() {
        return opposite;
    }

    public int getCounterClockwise() {
        return counterClockwise;
    }

    private final int center;
    private final int clockwise;
    private final int opposite;
    private final int counterClockwise;
}
