package ru.ntcap.core.neighbourhood;

import java.util.function.IntPredicate;

/**
 * This class represents Von Neumann neighbourhood which contains central cell and
 * its four adjacent neighbours.
 * @author Georgij Krajnyukov
 * @version 0.1.0
 */
public class VonNeumannNeighbourhood {
    public VonNeumannNeighbourhood(int center, int north, int west, int south, int east) {
        this.center = center;
        this.north = north;
        this.west = west;
        this.south = south;
        this.east = east;
    }

    /**
     * Counts neighbours, except for central one, that satisfies the predicate.
     * @param predicate predicate which returns true for cells that should be counted
     * @return number of neighbours satisfying the predicate
     */
    public int countIf(IntPredicate predicate) {
        int result = 0;
        if (predicate.test(north))
            ++result;
        if (predicate.test(west))
            ++result;
        if (predicate.test(south))
            ++result;
        if (predicate.test(east))
            ++result;
        return result;
    }

    public int getCenter() {
        return center;
    }

    public int getNorth() {
        return north;
    }

    public int getWest() {
        return west;
    }

    public int getSouth() {
        return south;
    }

    public int getEast() {
        return east;
    }

    private final int center;
    private final int north;
    private final int west;
    private final int south;
    private final int east;
}

