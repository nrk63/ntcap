package ru.ntcap.core.neighbourhood;

import java.util.function.IntPredicate;

/**
 * This class represents Moore neighbourhood which contains central cell and the
 * eight cells that surround it.
 * @author Georgij Krajnyukov
 * @version 0.1.0
 */
public class MooreNeighbourhood {
    public static class Builder {
        public Builder north(int north) {
            this.north = north;
            return this;
        }

        public Builder northWest(int northWest) {
            this.northWest = northWest;
            return this;
        }

        public Builder west(int west) {
            this.west = west;
            return this;
        }

        public Builder southWest(int southWest) {
            this.southWest = southWest;
            return this;
        }

        public Builder south(int south) {
            this.south = south;
            return this;
        }

        public Builder southEast(int southEast) {
            this.southEast = southEast;
            return this;
        }

        public Builder east(int east) {
            this.east = east;
            return this;
        }

        public Builder northEast(int northEast) {
            this.northEast = northEast;
            return this;
        }

        public Builder center(int center) {
            this.center = center;
            return this;
        }

        public MooreNeighbourhood build() {
            return new MooreNeighbourhood(this);
        }

        private int north = 0;
        private int northWest = 0;
        private int west = 0;
        private int southWest = 0;
        private int south = 0;
        private int southEast = 0;
        private int east = 0;
        private int northEast = 0;
        private int center = 0;
    }

    /**
     * Counts neighbours, except for central one, that satisfies the predicate.
     * @param predicate predicate which returns true for cells that should be counted
     * @return number of neighbours satisfying the predicate
     */
    public int countIf(IntPredicate predicate) {
        int result = 0;
        if (predicate.test(north))
            ++result;
        if (predicate.test(northWest))
            ++result;
        if (predicate.test(west))
            ++result;
        if (predicate.test(southWest))
            ++result;
        if (predicate.test(south))
            ++result;
        if (predicate.test(southEast))
            ++result;
        if (predicate.test(east))
            ++result;
        if (predicate.test(northEast))
            ++result;
        return result;
    }

    public int getCenter() {
        return center;
    }

    public int getNorth() {
        return north;
    }

    public int getNorthWest() {
        return northWest;
    }

    public int getWest() {
        return west;
    }

    public int getSouthWest() {
        return southWest;
    }

    public int getSouth() {
        return south;
    }

    public int getSouthEast() {
        return southEast;
    }

    public int getEast() {
        return east;
    }

    public int getNorthEast() {
        return northEast;
    }

    private final int center;
    private final int north;
    private final int northWest;
    private final int west;
    private final int southWest;
    private final int south;
    private final int southEast;
    private final int east;
    private final int northEast;

    private MooreNeighbourhood(Builder builder) {
        center = builder.center;
        north = builder.north;
        northWest = builder.northWest;
        west = builder.west;
        southWest = builder.southWest;
        south = builder.south;
        southEast = builder.southEast;
        east = builder.east;
        northEast = builder.northEast;
    }
}
