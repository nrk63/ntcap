/*NTCAP Copyright (C) 2021-2022  Georgij Krajnyukov <nrk63@yandex.ru>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package ru.ntcap.core;

import ru.ntcap.core.neighbourhood.MargolusNeighborhood;
import ru.ntcap.core.neighbourhood.MooreNeighbourhood;
import ru.ntcap.core.neighbourhood.VonNeumannNeighbourhood;

import java.util.Objects;
import java.util.function.IntBinaryOperator;
import java.util.function.IntUnaryOperator;

/**
 * This class provides a skeletal implementation of cellular planes system, designed to
 * minimize the effort required to implement any set of rules.
 * <br/><br/>
 * Two sets of planes are stores: <i>the planes</i> that are input data for the next step
 * and <i>temporary planes</i> that are storing new states, until current step ends.
 * <br/><br/>
 * To guarantee locality and oneness of rules, internal <i>current cell</i> (cells if there are
 * several planes) index is maintained, so derivative classes would not know on which cell they are
 * applying rules. Only supported neighbourhoods or that cell's state corresponding to the
 * <i>current index</i> could be received.
 * <br/><br/>
 * Default implementation of {@link #makeStep} method does:<br/>
 * <ol>
 *     <li>Sets the next index and calls {@link #applyRules} method, until all cells are processed.</li>
 *     <li>Copies new states from <i>temporary planes</i> to <i>the planes</i></li>
 *     <li>Increments step counter</li>
 * </ol>
 * To implement set of rules, {@link #applyRules} should be overridden.
 * <br/>
 * To change main cycle, {@link #makeStep()} should be overridden.
 * @author Georgij Krajnyukov
 * @version 0.4.0
 */
public abstract class AbstractCellularAutomaton implements CellularAutomaton,
                                                           CellularAutomatonView {
    /**
     * Makes a step. Applies rules to every cell, then copies <i>temporary planes</i>
     * to <i>the planes</i> and increments step counter.
     * @implSpec Uses {@link #applyRules} to process any cell and {@link #updatePlanes}
     * to copy <i>temporary planes</i> to <i>the planes</i>.
     */
    @Override
    public void makeStep() {
        for (currentIndex = 0; currentIndex < planesSize; ++currentIndex)
            applyRules();
        updatePlanes();
        ++step;
    }

    /**
     * Initializes plane of parameter index by applying initializer function on every cell,
     * passing cell's index as parameter.
     * @param planeIndex index of plane
     * @param initializer initializer function that returns cell state based on its index
     * @throws IndexOutOfBoundsException if plane of parameter index does not exist
     * @throws NullPointerException if initializer is {@code null}
     */
    @Override
    public void initPlane(int planeIndex, IntUnaryOperator initializer) {
        Objects.checkIndex(planeIndex, planes.length);
        Objects.requireNonNull(initializer);

        int[] plane = planes[planeIndex];
        for (int i = 0; i < planesSize; ++i)
            plane[i] = initializer.applyAsInt(i);
    }

    /**
     * Initializes plane of parameter index by applying initializer function on every cell,
     * passing cell's x and y coordinates as the first and the second parameters correspondingly.
     * @param planeIndex index of plane
     * @param initializer initializer function that returns cell state based on its index
     * @throws IndexOutOfBoundsException if plane of parameter index does not exist
     * @throws NullPointerException if initializer is {@code null}
     */
    @Override
    public void initPlane(int planeIndex, IntBinaryOperator initializer) {
        Objects.checkIndex(planeIndex, planes.length);
        Objects.requireNonNull(initializer);

        int[] plane = planes[planeIndex];
        for (int y = 0; y < height; ++y) {
            for (int x = 0; x < width; ++x) {
                plane[x + y*width] = initializer.applyAsInt(x, y);
            }
        }
    }

    /**
     * Gets width of planes.
     * @return width of planes
     */
    @Override
    public int getPlaneWidth() {
        return width;
    }

    /**
     * Gets height of planes.
     * @return height of planes
     */
    @Override
    public int getPlaneHeight() {
        return height;
    }

    /**
     * Gets number of the last complete step.
     * @return number of last complete step
     */
    public int getStep() {
        return step;
    }

    /**
     * Gets copy of a plane.
     * @param planeIndex index of plane
     * @return copy of a plane
     * @throws IndexOutOfBoundsException if plane of parameter index does not exist
     */
    @Override
    public int[] getPlane(int planeIndex) {
        int[] plane = planes[planeIndex];
        int[] result = new int[planesSize];
        System.arraycopy(plane, 0, result, 0, planesSize);
        return result;
    }

    @Override
    public int getPlanesCount() {
        return planes.length;
    }

    /**
     * Creates new instance with specified dimensions and planes count.
     * @param width planes width
     * @param height planes height
     * @param planesCount planes count
     * @throws IllegalArgumentException if width, height or number of planes is negative
     * @throws OutOfMemoryError if there is not enough memory to store planes of specified dimensions
     */
    protected AbstractCellularAutomaton(int width, int height, int planesCount) {
        if (width < 0)
            throw new IllegalArgumentException("negative width: " + width);
        if (height < 0)
            throw new IllegalArgumentException("negative height: " + height);
        if (planesCount < 0)
            throw new IllegalArgumentException("negative planes count" + planesCount);

        this.width = width;
        this.height = height;
        planesSize = width * height;

        planes = new int[planesCount][planesSize];
        tempPlanes = new int[planesCount][planesSize];
    }

    /**
     * Applies rules to the cell of <i>current index</i>.
     * @implNote
     * To commit a new state, {@link #toTempPlane(int, int)} should be used.
     * <br/>
     * To get current state, {@link #getState(int)} should be used.
     * <br/>
     * To get neighbourhood, eponymous method should be used. For example, {@link #mooreNeighbourhood(int)}.
     */
    protected abstract void applyRules();

    /**
     * Puts a state to <i>temporary plane</i> of parameter index.
     * @param planeIndex index of plane
     * @param state state to put
     * @throws IndexOutOfBoundsException if plane of parameter index does not exist
     */
    protected void toTempPlane(int planeIndex, int state) {
        tempPlanes[planeIndex][currentIndex] = state;
    }

    /**
     * Checks if <i>current cell</i> is on the upper edge of plane.
     * @return {@code true} only if <i>current cell</i> is on the upper edge of plane.
     */
    protected boolean isOnUpperEdge() {
        return currentIndex < width;
    }

    /**
     * Checks if <i>current cell</i> is on the bottom edge of plane.
     * @return {@code true} only if <i>current cell</i> is on the bottom edge of plane.
     */
    protected boolean isOnBottomEdge() {
        return currentIndex >= planesSize - width;
    }

    /**
     * Checks if <i>current cell</i> is on the left edge of plane.
     * @return {@code true} only if <i>current cell</i> is on the left edge of plane.
     */
    protected boolean isOnLeftEdge() {
        return currentIndex % width == 0;
    }

        /**
     * Checks if <i>current cell</i> is on the right edge of plane.
     * @return {@code true} only if <i>current cell</i> is on the right edge of plane.
     */
    protected boolean isOnRightEdge() {
        return currentIndex % width == width - 1;
    }

    /**
     * Copies temporary planes to the planes.
     */
    protected void updatePlanes() {
        for (int planeIndex = 0; planeIndex < planes.length; ++planeIndex) {
            System.arraycopy(tempPlanes[planeIndex], 0,
                             planes[planeIndex], 0, planesSize);
        }
    }

    /**
     * Returns Moore neighbourhood of <i>current cell</i>.
     * @param planeIndex index of plane
     * @return Moore neighbourhood of <i>current cell</i>
     */
    protected final MooreNeighbourhood mooreNeighbourhood(int planeIndex) {
        int[] plane = planes[planeIndex];

        int y = currentIndex / width;
        int x = currentIndex - y*width;

        int westX = (x - 1 + width) % width;
        int eastX = (x + 1) % width;

        int northY = ((y - 1 + height) % height) * width;
        int centerY = y * width;
        int southY = ((y + 1 + height) % height) * width;

        return mooreNeighbourhoodBuilder
            .northWest(plane[westX + northY])
            .north(plane[x + northY])
            .west(plane[westX + centerY])
            .southWest(plane[westX + southY])
            .south(plane[x + southY])
            .southEast(plane[eastX + southY])
            .east(plane[eastX + centerY])
            .northEast(plane[eastX + northY])
            .center(plane[currentIndex])
            .build();
    }

    /**
     * Returns Von Neumann neighbourhood of <i>current cell</i>.
     * @param planeIndex index of plane
     * @return Von Neumann neighbourhood of <i>current cell</i>
     */
    protected final VonNeumannNeighbourhood vonNeumannNeighbourhood(int planeIndex) {
        int[] plane = planes[planeIndex];
        int y = currentIndex / width;
        int x = currentIndex - y*width;

        int centerY = y * width;

        int center = plane[currentIndex];
        int north = plane[x + ((y - 1 + height) % height) * width];
        int west = plane[(x - 1 + width) % width + centerY];
        int south = plane[x + ((y + 1 + height) % height) * width];
        int east = plane[(x + 1) % width + centerY];

        return new VonNeumannNeighbourhood(center, north, west, south, east);
    }

    /**
     * Returns Margolus neighbourhood of <i>current cell</i>. Grid could be <i>even</i>,
     * that is (0; 0) is upper left corner, or <i>odd</i> - (0; 0) is lower right corner.
     * @param planeIndex index of plane
     * @param even true if grid is <i>even</i>, false if grid is <i>odd</i>.
     * @return Margolus neighbourhood of <i>current cell</i>
     */
    protected final MargolusNeighborhood margolusNeighborhood(int planeIndex, boolean even) {
        int[] plane = planes[planeIndex];
        int y = currentIndex / width;
        int x = currentIndex - y*width;

        //if even
        int UPPER_LEFT = 0; //->BOTTOM_RIGHT
        int UPPER_RIGHT = 1;//->LOWER_LEFT
        int LOWER_LEFT = 2;//->UPPER_RIGHT
        //int LOWER_RIGHT = 3;//->UPPER_LEFT

        int evenBit = even ? 0 : 1;
        int position = ((x + evenBit) & 1) + 2*((y + evenBit) & 1);

        int center = plane[currentIndex];
        int clockwise;
        int opposite;
        int counterClockwise;

        if (position == UPPER_LEFT) {
            int eastX = (x + 1) % width;
            int southY = ((y + 1 + height) % height) * width;
            clockwise = plane[eastX + y*width];
            opposite = plane[eastX + southY];
            counterClockwise = plane[x + southY];
        } else if (position == UPPER_RIGHT) {
            int westX = (x - 1 + width) % width;
            int southY = ((y + 1 + height) % height) * width;
            clockwise = plane[x + southY];
            opposite = plane[westX + southY];
            counterClockwise = plane[westX + y*width];
        } else if (position == LOWER_LEFT) {
            int northY = ((y - 1 + height) % height) * width;
            int eastX = (x + 1) % width;
            clockwise = plane[x + northY];
            opposite = plane[eastX + northY];
            counterClockwise = plane[eastX + y*width];
        } else {
            int northY = ((y - 1 + height) % height) * width;
            int westX = (x - 1 + width) % width;
            clockwise = plane[westX + y*width];
            opposite = plane[westX + northY];
            counterClockwise = plane[x + northY];
        }
        return new MargolusNeighborhood(center, clockwise, opposite, counterClockwise);
    }

    /**
     * Gets state of <i>current cell</i>.
     * @param planeIndex index of plane
     * @return state of cell of <i>current index</i>
     */
    protected int getState(int planeIndex) {
        return planes[planeIndex][currentIndex];
    }

    /**
     * width of planes
     */
    private final int width;

    /**
     * height of planes
     */
    private final int height;

    /**
     * total number of cell, which is {@code width * height}
     */
    private final int planesSize;

    /**
     * <i>the planes</i> that are input data for the next step
     */
    private final int[][] planes;

    /**
     * <i>temporary planes</i> that are storing new states, until current step ends
     */
    private final int[][] tempPlanes;

    /**
     * index of <i>current cell</i>
     */
    private int currentIndex;

    /**
     * step counter
     */
    private int step;

    private final MooreNeighbourhood.Builder mooreNeighbourhoodBuilder = new MooreNeighbourhood.Builder();
}
