/*NTCAP Copyright (C) 2021-2022  Georgij Krajnyukov <nrk63@yandex.ru>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package ru.ntcap.core.recipes;

import ru.ntcap.core.AbstractCellularAutomaton;
import ru.ntcap.core.neighbourhood.VonNeumannNeighbourhood;

/**
 * This class implements rules of falling cells, which become still, when fallen
 * on plane 1 "barrier".
 * <br/>
 * Example: to plot histogram, initialize barrier as (x, height - 1) line, where x&isin;[0; width).
 * Plane 0 could be initialized randomly.
 * @author Georgij Krajnyukov
 * @version 0.1.0
 */
public class Histogram extends AbstractCellularAutomaton {
    public Histogram(int width, int height) {
        super(width, height, 2);
    }

    @Override
    protected void applyRules() {
        VonNeumannNeighbourhood neighbourhood0 = vonNeumannNeighbourhood(0);
        VonNeumannNeighbourhood neighbourhood1 = vonNeumannNeighbourhood(1);
        int center0 = neighbourhood0.getCenter();
        int center1 = neighbourhood1.getCenter();

        if (center0 == 0 && neighbourhood0.getNorth() != 0
                && !(center1 != 0 && neighbourhood1.getNorth() == 0)) {
            toTempPlane(0, neighbourhood0.getNorth());
        } else if (center0 != 0 && neighbourhood0.getSouth() == 0
                && !(center1 == 0 && neighbourhood1.getSouth() != 0)) {
           toTempPlane(0, 0);
        } else {
            toTempPlane(0, center0);
        }
        toTempPlane(1, center1);
    }
}
