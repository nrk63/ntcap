/*NTCAP Copyright (C) 2021-2022  Georgij Krajnyukov <nrk63@yandex.ru>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package ru.ntcap.core.recipes;

import ru.ntcap.core.AbstractCellularAutomaton;

/**
 * This "Scarves" rule implementation shifts plane twice to access west-of-the-west
 * and east-of-the-east neighbours.
 * <p>
 * Scarves is 1D rule that inverts cell if it's extended neighbourhood consists of 2 ones
 * and 2 zeros. Previous states are saved in north row.
 * </p>
 * @author Georgij Krajnyukov
 * @version 0.1.0
 */
public class Scarves extends AbstractCellularAutomaton {
    public Scarves(int width, int height) {
        super(width, height, 3);
    }

    /**
     * Applies rules in two steps. At even steps shifts plane twice, placing result in
     * another planes. At odd steps applies "Scarves" rule.
     */
    @Override
    public void makeStep() {
        super.makeStep();
        shifting = !shifting;
    }

    @Override
    protected void applyRules() {
        var neighbourhood = vonNeumannNeighbourhood(0);
        if (shifting) {
            //Shift to the left
            toTempPlane(1, neighbourhood.getEast());
            //Shift to the right
            toTempPlane(2, neighbourhood.getWest());
            //Nothing changes during this phase
            toTempPlane(0, neighbourhood.getCenter());
        } else if (isOnBottomEdge()) {
            var leftShiftedPast = vonNeumannNeighbourhood(1);
            var rightShiftedPast = vonNeumannNeighbourhood(2);
            //west-west + west + east + east-east
            /*
             * Note that, according to the shifting phase,
             * rightShiftedPast.getWest() is equal to neighbourhood.getWest().getWest()
             * and
             * leftShiftedPast.getEast() is equal to neighbourhood.getEast().getEast()
             */
            int neighboursCount = rightShiftedPast.getWest() + neighbourhood.getWest()
                                  + neighbourhood.getEast() + leftShiftedPast.getEast();
            toTempPlane(0, (neighboursCount == 2 ? 1 : 0) ^ neighbourhood.getNorth());
        //Not shifting
        } else {
            //Save the past in north cells
            toTempPlane(0, neighbourhood.getSouth());
        }
    }

    /**
     * flag indicating if this step is shifting phase
     */
    private boolean shifting = true;
}
