/*NTCAP Copyright (C) 2021-2022  Georgij Krajnyukov <nrk63@yandex.ru>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package ru.ntcap.core.recipes;

import ru.ntcap.core.AbstractCellularAutomaton;

import java.util.Random;

/**
 * <p>
 *  This class implements Cyclic Rank rule. Cells change states (ranks) within specified
 *  cycled range: from 0 to {@code statesCount}. In addition, cells promoted in rank
 *  only when one randomly chosen neighbor is exactly one rank higher.
 * </p>
 * <p>
 *  In that case, each cell may be considered as a phase variable of 1D rank phase space.
 * </p>
 * <p>
 *  Note that <i>asynchronously</i> implies Poisson point process. In this implementation,
 *  there is no guarantee of such events as {@code Random} is used without any justification.
 * </p>
 *
 * @author Georgij Krajnyukov
 * @version 0.1.0
 */
public class CyclicRank extends AbstractCellularAutomaton {
    public CyclicRank(int width, int height, int statesCount, double probability) {
        super(width, height, 1);
        this.statesCount = statesCount;
        this.probability = probability;
        this.noiseGenerator = new Random();
    }

    public int getStatesCount() {
        return statesCount;
    }

    @Override
    protected void applyRules() {
        if (!(noiseGenerator.nextDouble()*100.0 <= probability)) {
            toTempPlane(0, getState(0));
            return;
        }

        var neighbourhood = vonNeumannNeighbourhood(0);

        int strongerNeighbour = (neighbourhood.getCenter() + 1) % statesCount;
        if (strongerNeighbour == neighbourhood.getNorth() ||
            strongerNeighbour == neighbourhood.getWest() ||
            strongerNeighbour == neighbourhood.getSouth() ||
            strongerNeighbour == neighbourhood.getEast()) {
            toTempPlane(0, strongerNeighbour);
        } else {
            toTempPlane(0, neighbourhood.getCenter());
        }
    }

    private final int statesCount;
    private final double probability;
    private final Random noiseGenerator;
}
