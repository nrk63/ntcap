/*NTCAP Copyright (C) 2021-2022  Georgij Krajnyukov <nrk63@yandex.ru>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package ru.ntcap.core.recipes;

import ru.ntcap.core.AbstractCellularAutomaton;

/**
 * This "Scarves" rule implementation uses "pleated" planes to access west-of-the-west and
 * east-of-the-east neighbours. Previous states are saved in north rows.
 * <p>
 * Scarves is 1D rule that inverts cell if it's extended neighbourhood consists of 2 ones
 * and 2 zeros.
 * </p>
 * <p>
 * Pleating approach of accessing "west-west" and "east-east" is to rearrange one plane
 * in two planes, placing each second cell "under" each first.
 * </p>
 * <p>
 * For example, if original plane is:<br/>
 * a b c d e f g h<br/>
 * Then pleating planes are:<br/>
 * 0: a c e g<br/>
 * 1: b d f h
 * </p>
 * <p>
 * Thus, "west-west" is west, "west" is west', "east" is center' and "east-east" is east.
 * It also means on each step rule must be applied to both centers.
 * </p>
 * @author Georgij Krajnyukov
 * @version 0.1.0
 */
public class PleatedScarves extends AbstractCellularAutomaton {
    public PleatedScarves(int width, int height) {
        super(width, height, 2);
    }

    @Override
    protected void applyRules() {
        var neighbourhood0 = vonNeumannNeighbourhood(0);
        var neighbourhood1 = vonNeumannNeighbourhood(1);
        //If now is now
        if (isOnBottomEdge()) {
            //west-west + west + east + east-east
            int neighboursCount0 = neighbourhood0.getWest() + neighbourhood1.getWest()
                                       + neighbourhood1.getCenter() + neighbourhood0.getEast();
            int neighboursCount1 = neighbourhood1.getWest() + neighbourhood0.getCenter()
                                       + neighbourhood0.getEast() + neighbourhood1.getEast();
            neighboursCount0 = neighboursCount0 == 2 ? 1 : 0;
            neighboursCount1 = neighboursCount1 == 2 ? 1 : 0;
            toTempPlane(0, neighboursCount0 ^ neighbourhood0.getNorth());
            toTempPlane(1, neighboursCount1 ^ neighbourhood1.getNorth());
        } else {
            //Save the past in north cells
            toTempPlane(0, neighbourhood0.getSouth());
            toTempPlane(1, neighbourhood1.getSouth());
        }
    }
}
