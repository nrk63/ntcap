/*NTCAP Copyright (C) 2021-2022  Georgij Krajnyukov <nrk63@yandex.ru>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package ru.ntcap.core.recipes;

import ru.ntcap.core.AbstractCellularAutomaton;

public class MyTubeWorms extends AbstractCellularAutomaton {
    public MyTubeWorms(int width, int height) {
        super(width, height, 3);
    }

    @Override
    protected void applyRules() {
        var neighbourhood = mooreNeighbourhood(0);
        int neighboursCount = neighbourhood.countIf((int state) -> state == 1);
        int countdown = getState(2);

        toTempPlane(0, WORM[countdown]);
        toTempPlane(1, ALARM[neighboursCount] ? 1 : 0);
        if (neighbourhood.getCenter() == 1 && getState(1) == 1)
            toTempPlane(2, TIMER.length - 1);
        else
            toTempPlane(2, TIMER[countdown]);
    }

    //1100
    private final int[] WORM = new int[] {1, 1, 0, 0};
    private final int[] TIMER = new int[] {0, 0, 1, 2};
    //from 3 to 5
    //from 4 to 6
    //from 5 to 7
    //7-8
    //0, 8
    //fibonacci
    //all true except 1
    private final boolean[] ALARM = new boolean[] {
        //false, false, false, false, false, false, false, false, false
        true, false, false, false, false, false, false, false, true
    };
}
