/*NTCAP Copyright (C) 2021-2022  Georgij Krajnyukov <nrk63@yandex.ru>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package ru.ntcap.core.recipes;

import ru.ntcap.core.AbstractCellularAutomaton;
import ru.ntcap.core.neighbourhood.MargolusNeighborhood;

/**
 * This class implements rules of "gas" cells, which move horizontally or vertically
 * and when collided, start to move in opposites direction, so that total momentum remains
 * constant.
 * @author Georgij Krajnyukov
 * @version 0.1.0
 */
public class Gas extends AbstractCellularAutomaton {
    public Gas(int width, int height) {
        super(width, height, 1);
        even = true;
    }

    @Override
    protected void applyRules() {
        MargolusNeighborhood neighborhood = margolusNeighborhood(0, even);
        int center = neighborhood.getCenter();
        int clockwise = neighborhood.getClockwise();
        int opposite = neighborhood.getOpposite();
        int counterClockwise = neighborhood.getCounterClockwise();

        if (center == opposite && clockwise == counterClockwise && center != clockwise)
            toTempPlane(0, clockwise);
        else
            toTempPlane(0, opposite);
    }

    @Override
    public void makeStep() {
        super.makeStep();
        even = !even;
    }

    private boolean even;
}
