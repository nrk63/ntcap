/*NTCAP Copyright (C) 2021-2022  Georgij Krajnyukov <nrk63@yandex.ru>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package ru.ntcap.core.recipes;

import ru.ntcap.core.AbstractCellularAutomaton;
import ru.ntcap.core.neighbourhood.MooreNeighbourhood;

/**
 * This class implements basic Conway's Game of Life rules.
 * @author Georgij Krajnyukov
 * @version 0.1.0
 */
public class Life extends AbstractCellularAutomaton {
    public Life(int width, int height) {
        super(width, height, 1);
    }

    @Override
    protected void applyRules() {
        MooreNeighbourhood neighbourhood = mooreNeighbourhood(0);
        int neighboursCount = neighbourhood.countIf((int state) -> state == 1);
        int center = neighbourhood.getCenter();

        if ((center == 1 && (neighboursCount == 2 || neighboursCount == 3))
            || (center == 0 && neighboursCount == 3)) {
            toTempPlane(0, 1);
        } else {
            toTempPlane(0, 0);
        }
    }
}
