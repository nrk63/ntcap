package ru.ntcap.core.recipes;

import ru.ntcap.core.AbstractCellularAutomaton;

public class Parity extends AbstractCellularAutomaton {
    public Parity(int width, int height) {
        super(width, height, 1);
    }

    protected void applyRules() {
        var neighbourhood = vonNeumannNeighbourhood(0);
        toTempPlane(0,neighbourhood.getCenter()
                                    ^ neighbourhood.getNorth()
                                    ^ neighbourhood.getSouth()
                                    ^ neighbourhood.getWest()
                                    ^ neighbourhood.getEast());

    }
}
