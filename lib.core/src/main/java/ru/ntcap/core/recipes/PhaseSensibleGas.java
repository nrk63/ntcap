package ru.ntcap.core.recipes;

import ru.ntcap.core.AbstractCellularAutomaton;
import ru.ntcap.core.neighbourhood.MargolusNeighborhood;

public class PhaseSensibleGas extends AbstractCellularAutomaton {
    private boolean phase;

    public PhaseSensibleGas(int width, int height) {
        super(width, height, 1);
        phase = true;
    }

    @Override
    protected void applyRules() {
        MargolusNeighborhood neighborhood = margolusNeighborhood(0, phase);
        int center = neighborhood.getCenter();
        int clockwise = neighborhood.getClockwise();
        int opposite = neighborhood.getOpposite();
        int counterClockwise = neighborhood.getCounterClockwise();

        if (center == opposite && clockwise == counterClockwise && center != clockwise) {
            toTempPlane(0, center);
        } else {
            if (phase)
                toTempPlane(0, clockwise);
            else
                toTempPlane(0, counterClockwise);
        }

    }

    @Override
    public void makeStep() {
        super.makeStep();
        phase = !phase;
    }
}
