package ru.ntcap.core;

import ru.ntcap.core.recipes.TubeWorms;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Arrays;
import java.util.stream.Stream;

public class TubeWormsTests {
    private static final int CENTER = 4;

    record TestParams(int[] plane0, int[] plane1, int[] plane2,
                      int expected0, int expected1, int expected2) {
        @Override
        public String toString() {
            return "TestParam{" +
                    "plane0=" + Arrays.toString(plane0) +
                    ", plane1=" + Arrays.toString(plane1) +
                    ", plane2=" + Arrays.toString(plane2) +
                    ", expected0=" + expected0 +
                    ", expected1=" + expected1 +
                    ", expected2=" + expected2 +
                    '}';
        }
    }

    @ParameterizedTest
    @MethodSource("getTubeWormsAlarmStart1Params")
    void Positive_ApplyRules_AlarmStart1_PatternsMatch(TestParams params) {
        TubeWorms rule = new TubeWorms(3, 3, 1);
        rule.initPlane(0, i -> params.plane0[i]);
        rule.initPlane(1, i -> params.plane1[i]);
        rule.initPlane(2, i -> params.plane2[i]);

        rule.makeStep();

        int[] resultPlane0 = rule.getPlane(0);
        int[] resultPlane1 = rule.getPlane(1);
        int[] resultPlane2 = rule.getPlane(2);
        Assertions.assertEquals(params.expected0, resultPlane0[CENTER], "CENTER 0 doesn't match");
        Assertions.assertEquals(params.expected1, resultPlane1[CENTER], "CENTER 1 doesn't match");
        Assertions.assertEquals(params.expected2, resultPlane2[CENTER], "CENTER 2 doesn't match");
    }

    private static Stream<TestParams> getTubeWormsAlarmStart1Params() {
        return Stream.of(
            //                          N     W  C  E     S
            // No one is around, so worm sticks out
            new TestParams(new int[]{ 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                          new int[]{ 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                          new int[]{ 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                          1, 0, 0),
            // No one is around, so worm doesn't hide
            new TestParams(new int[]{ 0, 0, 0, 0, 1, 0, 0, 0, 0 },
                          new int[]{ 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                          new int[]{ 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                          1, 0, 0),
            // No one is around, alarm is set on, but timer's off
            //     => worm doesn't hide, alarm sets off, timer is started
            new TestParams(new int[]{ 0, 0, 0, 0, 1, 0, 0, 0, 0 },
                          new int[]{ 0, 0, 0, 0, 1, 0, 0, 0, 0 },
                          new int[]{ 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                          1, 0, 3),
            // Timer is started, alarm is off => worm hides;
            // no danger, so alarm is off, timer ticks
            new TestParams(new int[]{ 0, 0, 0, 0, 1, 0, 0, 0, 0 },
                          new int[]{ 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                          new int[]{ 0, 0, 0, 0, 3, 0, 0, 0, 0 },
                          0, 0, 2),
            // Timer is started, alarm is on == time for worm to hide
            // (alarms sets off, cause there is no danger), timer starts again
            new TestParams(new int[]{ 0, 0, 0, 0, 1, 0, 0, 0, 0 },
                          new int[]{ 0, 0, 0, 0, 1, 0, 0, 0, 0 },
                          new int[]{ 0, 0, 0, 0, 3, 0, 0, 0, 0 },
                          0, 0, 3),
            // Timer continues to tick (from 2 to 1)
            new TestParams(new int[]{ 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                          new int[]{ 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                          new int[]{ 0, 0, 0, 0, 2, 0, 0, 0, 0 },
                          0, 0, 1),
            // Timer continues to tick (from 1 to 0)
            new TestParams(new int[]{ 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                          new int[]{ 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                          new int[]{ 0, 0, 0, 0, 1, 0, 0, 0, 0 },
                          0, 0, 0),
            // Timer ticks despite worm is visible  (from 2 to 0)
            // and worm hides, cause timer isn't zero
            new TestParams(new int[]{ 0, 0, 0, 0, 1, 0, 0, 0, 0 },
                          new int[]{ 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                          new int[]{ 0, 0, 0, 0, 2, 0, 0, 0, 0 },
                          0, 0, 1),
            // Timer ticks despite worm is visible  (from 1 to 0)
            // but still worm must hide
            new TestParams(new int[]{ 0, 0, 0, 0, 1, 0, 0, 0, 0 },
                          new int[]{ 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                          new int[]{ 0, 0, 0, 0, 1, 0, 0, 0, 0 },
                          0, 0, 0),
            // There is a danger, alarm turns on, but worm still calm
            new TestParams(new int[]{ 1, 0, 0, 0, 1, 0, 0, 0, 0 },
                          new int[]{ 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                          new int[]{ 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                          1, 1, 0),
            new TestParams(new int[]{ 1, 1, 0, 0, 1, 0, 0, 0, 0 },
                          new int[]{ 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                          new int[]{ 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                          1, 1, 0),
            new TestParams(new int[]{ 1, 1, 1, 0, 1, 0, 0, 0, 0 },
                          new int[]{ 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                          new int[]{ 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                          1, 1, 0),
            new TestParams(new int[]{ 1, 1, 1, 1, 1, 0, 0, 0, 0 },
                          new int[]{ 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                          new int[]{ 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                          1, 1, 0),
            new TestParams(new int[]{ 1, 1, 1, 1, 1, 1, 0, 0, 0 },
                          new int[]{ 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                          new int[]{ 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                          1, 1, 0),
            new TestParams(new int[]{ 1, 1, 1, 1, 1, 1, 1, 0, 0 },
                          new int[]{ 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                          new int[]{ 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                          1, 1, 0),
            new TestParams(new int[]{ 1, 1, 1, 1, 1, 1, 1, 1, 0 },
                          new int[]{ 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                          new int[]{ 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                          1, 1, 0),
            new TestParams(new int[]{ 1, 1, 1, 1, 1, 1, 1, 1, 1 },
                          new int[]{ 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                          new int[]{ 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                          1, 1, 0),
            // There is the danger, but worm shows up, because
            // timer is set to 0
            new TestParams(new int[]{ 1, 0, 0, 0, 0, 0, 0, 0, 0 },
                          new int[]{ 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                          new int[]{ 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                          1, 1, 0)
        );
    }
}
