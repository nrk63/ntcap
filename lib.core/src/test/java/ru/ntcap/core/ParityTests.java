package ru.ntcap.core;

import ru.ntcap.core.recipes.Parity;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Arrays;
import java.util.stream.Stream;

public class ParityTests {
    private static final int CENTER = 4;

    record TestParams(int[] plane, int expected) {
        @Override
        public String toString() {
            return "TestParam{" +
                    "plane=" + Arrays.toString(plane) +
                    ", expected=" + expected +
                    '}';
        }
    }

    @ParameterizedTest
    @MethodSource("getTestParams")
    void Positive_ApplyRules_PatternsMatch(TestParams params) {
        Parity parity = new Parity(3, 3);
        parity.initPlane(0, i -> params.plane[i]);

        parity.makeStep();

        int[] plane = parity.getPlane(0);
        Assertions.assertEquals(params.expected, plane[CENTER], "CENTER doesn't match");
    }

    private static final int X = 0;
    private static Stream<TestParams> getTestParams() {
        return Stream.of(
            //                           N     W  C  E     S
            new TestParams(new int[]{ X, 0, X, 0, 0, 0, X, 0, X }, 0),
            new TestParams(new int[]{ X, 0, X, 0, 0, 0, X, 1, X }, 1),
            new TestParams(new int[]{ X, 0, X, 0, 0, 1, X, 0, X }, 1),
            new TestParams(new int[]{ X, 0, X, 0, 0, 1, X, 1, X }, 0),
            new TestParams(new int[]{ X, 0, X, 0, 1, 0, X, 0, X }, 1),
            new TestParams(new int[]{ X, 0, X, 0, 1, 0, X, 1, X }, 0),
            new TestParams(new int[]{ X, 0, X, 0, 1, 1, X, 0, X }, 0),
            new TestParams(new int[]{ X, 0, X, 0, 1, 1, X, 1, X }, 1),
            new TestParams(new int[]{ X, 0, X, 1, 0, 0, X, 0, X }, 1),
            new TestParams(new int[]{ X, 0, X, 1, 0, 0, X, 1, X }, 0),
            new TestParams(new int[]{ X, 0, X, 1, 0, 1, X, 0, X }, 0),
            new TestParams(new int[]{ X, 0, X, 1, 0, 1, X, 1, X }, 1),
            new TestParams(new int[]{ X, 0, X, 1, 1, 0, X, 0, X }, 0),
            new TestParams(new int[]{ X, 0, X, 1, 1, 0, X, 1, X }, 1),
            new TestParams(new int[]{ X, 0, X, 1, 1, 1, X, 0, X }, 1),
            new TestParams(new int[]{ X, 0, X, 1, 1, 1, X, 1, X }, 0),
            new TestParams(new int[]{ X, 1, X, 0, 0, 0, X, 0, X }, 1),
            new TestParams(new int[]{ X, 1, X, 0, 0, 0, X, 1, X }, 0),
            new TestParams(new int[]{ X, 1, X, 0, 0, 1, X, 0, X }, 0),
            new TestParams(new int[]{ X, 1, X, 0, 0, 1, X, 1, X }, 1),
            new TestParams(new int[]{ X, 1, X, 0, 1, 0, X, 0, X }, 0),
            new TestParams(new int[]{ X, 1, X, 0, 1, 0, X, 1, X }, 1),
            new TestParams(new int[]{ X, 1, X, 0, 1, 1, X, 0, X }, 1),
            new TestParams(new int[]{ X, 1, X, 0, 1, 1, X, 1, X }, 0),
            new TestParams(new int[]{ X, 1, X, 1, 0, 0, X, 0, X }, 0),
            new TestParams(new int[]{ X, 1, X, 1, 0, 0, X, 1, X }, 1),
            new TestParams(new int[]{ X, 1, X, 1, 0, 1, X, 0, X }, 1),
            new TestParams(new int[]{ X, 1, X, 1, 0, 1, X, 1, X }, 0),
            new TestParams(new int[]{ X, 1, X, 1, 1, 0, X, 0, X }, 1),
            new TestParams(new int[]{ X, 1, X, 1, 1, 0, X, 1, X }, 0),
            new TestParams(new int[]{ X, 1, X, 1, 1, 1, X, 0, X }, 0),
            new TestParams(new int[]{ X, 1, X, 1, 1, 1, X, 1, X }, 1)
        );
    }
}
