plugins {
    application
}

application {
    mainClass.set("ru.ntcap.desktop.App")
}

java {
    sourceCompatibility = JavaVersion.VERSION_17
    targetCompatibility = JavaVersion.VERSION_17
}

tasks.test {
    useJUnitPlatform()
}

dependencies {
    implementation(project(":lib.core"))

    implementation(libs.guava)

    testImplementation(libs.jupiter.api)
    testRuntimeOnly(libs.jupiter.engine)

    testImplementation(libs.mockito.core)
    testImplementation(libs.mockito.junitJupiter)
}
