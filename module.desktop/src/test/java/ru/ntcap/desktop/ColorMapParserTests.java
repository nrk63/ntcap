package ru.ntcap.desktop;

import ru.ntcap.desktop.colormap.ColorMap;
import ru.ntcap.desktop.colormap.ColorMapParser;
import ru.ntcap.desktop.filesystem.FileSystemFactory;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;

import java.nio.file.Path;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

public class ColorMapParserTests {
    @AfterEach
    void tearDown() {
        FileSystemFactory.reset();
    }

    @Test
    public void Positive_TwoCanonicalLines_SuccessfullyParse() {
        FileSystemFactory.setInstance(path -> List.of(
            "0 -> 0x012389",
            "1 -> 0xABCDEF"
        ));

        try {
            Map<Integer, Integer> map = ColorMapParser.parse(Path.of("test1"));
            assertEquals(map.get(0), 0x012389);
            assertEquals(map.get(1), 0xABCDEF);
        } catch (Exception e) {
            fail("Unexpected exception was thrown: " + e.getMessage());
        }
    }

    @Test
    public void Positive_TwoCanonicalLinesWithEmptyLineBetween_SuccessfullyParse() {
        FileSystemFactory.setInstance(path -> List.of(
            "0 -> 0x777888",
            "",
            "1 -> 0xAABBCC"
        ));

        try {
            Map<Integer, Integer> map = ColorMapParser.parse(Path.of("test2"));
            assertEquals(map.get(0), 0x777888);
            assertEquals(map.get(1), 0xAABBCC);
        } catch (Exception e) {
            fail("Unexpected exception was thrown: " + e.getMessage());
        }
    }
}
