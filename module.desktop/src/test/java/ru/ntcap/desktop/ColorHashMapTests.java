package ru.ntcap.desktop;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import ru.ntcap.desktop.colormap.ColorHashMap;

import java.util.Map;
import java.util.NoSuchElementException;

public class ColorHashMapTests {
    @Test
    void Positive_GetRgbOf_MapWith3Colors_ReturnsCorrectColors() {
        Map<Integer, Integer> testColors = Map.of(
            -1, 0x00FF00,
            0, 0xCC0000,
            3, 0xFFEECC
        );
        ColorHashMap colorMap = new ColorHashMap(testColors);

        Assertions.assertEquals(testColors.get(-1), colorMap.getRgbOf(-1));
        Assertions.assertEquals(testColors.get(0), colorMap.getRgbOf(0));
        Assertions.assertEquals(testColors.get(3), colorMap.getRgbOf(3));
    }

    @Test
    void Negative_GetRgbOf_EmptyMap_Throws() {
        ColorHashMap colorMap = new ColorHashMap(Map.of());
        Assertions.assertThrows(NoSuchElementException.class,
                               () -> colorMap.getRgbOf(0));
    }
}
