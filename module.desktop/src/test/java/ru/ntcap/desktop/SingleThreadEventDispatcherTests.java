package ru.ntcap.desktop;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import ru.ntcap.desktop.events.Event;
import ru.ntcap.desktop.events.EventHandler;
import ru.ntcap.desktop.events.SingleThreadEventDispatcher;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayDeque;
import java.util.Queue;

@ExtendWith(MockitoExtension.class)
public class SingleThreadEventDispatcherTests {
    @Mock
    EventHandler mockEventHandler;

    @Test
    void Positive_AddEventHandler_AfterAddingOne_ReturnsMapContainingHandler() {
        String testEventName = "test 1";
        EventHandler testEventHandler = e -> {};

        SingleThreadEventDispatcher dispatcher = new SingleThreadEventDispatcher();
        dispatcher.addEventHandler(testEventName, testEventHandler);

        Multimap<String, EventHandler> handlers = dispatcher.copyEventHandlers();
        Assertions.assertTrue(handlers.containsEntry(testEventName, testEventHandler));
    }

    @Test
    void Positive_RemoveEventHandler_AfterAddingOne_ReturnsEmptyMapOfHandlers() {
        String testEventName = "test 2";
        EventHandler testEventHandler = e -> {};

        SingleThreadEventDispatcher dispatcher = new SingleThreadEventDispatcher();
        dispatcher.addEventHandler(testEventName, testEventHandler);
        dispatcher.removeEventHandler(testEventName, testEventHandler);

        Multimap<String, EventHandler> handlers = dispatcher.copyEventHandlers();
        Assertions.assertTrue(handlers.isEmpty());
    }

    @Test
    public void Positive_EnqueueEvent_OneEventEnqueued_ReturnsQueueWithThisEvent() {
        Event testEvent = new Event("test-enqueue-event");

        SingleThreadEventDispatcher dispatcher = new SingleThreadEventDispatcher();
        dispatcher.enqueueEvent(testEvent);

        Queue<Event> events = dispatcher.copyAndClearEventQueue();
        Assertions.assertTrue(events.contains(testEvent));
    }

    @Test
    public void Positive_EnqueueEvent_TwoEventsEnqueued_ReturnsQueueWithSameSequence() {
        Event testEvent1 = new Event("test-queue-sequence 1");
        Event testEvent2 = new Event("test-queue-sequence 2");

        SingleThreadEventDispatcher dispatcher = new SingleThreadEventDispatcher();
        dispatcher.enqueueEvent(testEvent1);
        dispatcher.enqueueEvent(testEvent2);

        Queue<Event> events = dispatcher.copyAndClearEventQueue();
        Assertions.assertEquals(2, events.size());
        Event firstEvent = events.poll();
        Event secondEvent = events.poll();
        Assertions.assertEquals(testEvent1, firstEvent);
        Assertions.assertEquals(testEvent2, secondEvent);
    }

    @Test
    public void Positive_CopyAndClearEventQueue_OneEventEnqueued_ReturnsEmptyQueueAfterFirstCall() {
        Event testEvent = new Event("test-enqueue-event");

        SingleThreadEventDispatcher dispatcher = new SingleThreadEventDispatcher();
        dispatcher.enqueueEvent(testEvent);

        dispatcher.copyAndClearEventQueue();
        Queue<Event> events = dispatcher.copyAndClearEventQueue();
        Assertions.assertTrue(events.isEmpty());
    }

    @Test
    void Positive_Dispatch_EmptyEventQueue_DoesNothing() {
        Queue<Event> testEvents = new ArrayDeque<>();
        Multimap<String, EventHandler> testEventHandlers = ArrayListMultimap.create(1, 1);
        testEventHandlers.put("test-event-for-dispatch", e -> {});

        Assertions.assertDoesNotThrow(() -> SingleThreadEventDispatcher.dispatch(testEvents, testEventHandlers));
    }

    @Test
    void Positive_Dispatch_EmptyEventHandlers_DoesNothing() {
        Queue<Event> testEvents = new ArrayDeque<>();
        testEvents.add(new Event("aaa-bbb"));
        Multimap<String, EventHandler> testEventHandlers = ArrayListMultimap.create();

        Assertions.assertDoesNotThrow(() -> SingleThreadEventDispatcher.dispatch(testEvents, testEventHandlers));
    }

    @Test
    void Positive_Dispatch_TwoHandlersHandlingDifferentEvents_EachReceivesItsOwn() {
        EventHandler mockEventHandler1 = Mockito.mock(EventHandler.class);

        Event testEvent1 = new Event("for 1");
        Event testEvent2 = new Event("for 2");

        SingleThreadEventDispatcher dispatcher = new SingleThreadEventDispatcher();
        dispatcher.addEventHandler(testEvent1.getName(), mockEventHandler);
        dispatcher.addEventHandler(testEvent2.getName(), mockEventHandler1);
        dispatcher.enqueueEvent(testEvent1);
        dispatcher.enqueueEvent(testEvent2);

        Queue<Event> events = dispatcher.copyAndClearEventQueue();
        Multimap<String, EventHandler> handlers = dispatcher.copyEventHandlers();
        SingleThreadEventDispatcher.dispatch(events, handlers);

        Mockito.verify(mockEventHandler).handleEvent(testEvent1);
        Mockito.verify(mockEventHandler1).handleEvent(testEvent2);
    }
}
