package ru.ntcap.desktop;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import ru.ntcap.desktop.colormap.SafeColorHashMap;

import java.util.Map;
import java.util.function.Supplier;

public class SafeColorHashMapTests {
    @Test
    void Positive_getRgbOf_NoInitColors_ReturnsWhatSupplierSupplies() {
        final int testColor = 0xFF0000;
        Supplier<Integer> testSupplier = () -> testColor;
        SafeColorHashMap colorMap = new SafeColorHashMap(Map.of(), testSupplier);
        Assertions.assertEquals(testColor, colorMap.getRgbOf(0));
        Assertions.assertEquals(testColor, colorMap.getRgbOf(-34));
        Assertions.assertEquals(testColor, colorMap.getRgbOf(55));
    }

    @Test
    void Positive_getRgbOf_TwoInitColors_UsesInitColorsElseGeneratesNew() {
        final int testColor = 0xCCCCCC;
        Supplier<Integer> testSupplier = () -> testColor;
        Map<Integer, Integer> testInitColors = Map.of(
            0, 0x00FFF0,
            5, 0x0FFF00,
            -3, 0xF0000F
        );

        SafeColorHashMap colorMap = new SafeColorHashMap(testInitColors, testSupplier);
        Assertions.assertEquals(testInitColors.get(0), colorMap.getRgbOf(0));
        Assertions.assertEquals(testInitColors.get(5), colorMap.getRgbOf(5));
        Assertions.assertEquals(testInitColors.get(-3), colorMap.getRgbOf(-3));
        Assertions.assertEquals(testColor, colorMap.getRgbOf(4));
    }
}
