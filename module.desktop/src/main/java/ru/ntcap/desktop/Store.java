package ru.ntcap.desktop;

import ru.ntcap.core.CellularAutomaton;
import ru.ntcap.desktop.recipes.DefaultPlaneParamsStore;

public class Store implements DefaultPlaneParamsStore, CellularAutomatonStore {
    @Override
    public int getDefaultPlaneWidth() {
        return defaultPlaneWidth;
    }

    @Override
    public int getDefaultPlaneHeight() {
        return defaultPlaneHeight;
    }

    @Override
    public CellularAutomaton getCellularAutomaton() {
        return cellularAutomaton;
    }

    @Override
    public String getRecipeName() {
        return recipeName;
    }

    public void setDefaultPlaneWidth(int defaultPlaneWidth) {
        this.defaultPlaneWidth = defaultPlaneWidth;
    }

    public void setDefaultPlaneHeight(int defaultPlaneHeight) {
        this.defaultPlaneHeight = defaultPlaneHeight;
    }

    public void setCellularAutomaton(CellularAutomaton cellularAutomaton) {
        this.cellularAutomaton = cellularAutomaton;
    }

    public void setRecipeName(String recipeName) {
        this.recipeName = recipeName;
    }

    private int defaultPlaneWidth;
    private int defaultPlaneHeight;
    private CellularAutomaton cellularAutomaton;
    private String recipeName;
}
