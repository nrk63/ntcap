package ru.ntcap.desktop;

import ru.ntcap.core.CellularAutomaton;
import ru.ntcap.core.recipes.CyclicRank;
import ru.ntcap.desktop.colormap.*;
import ru.ntcap.desktop.colormap.ColorMapParser;
import ru.ntcap.desktop.colormap.HueShiftColorSupplier;
import ru.ntcap.desktop.events.Event;
import ru.ntcap.desktop.events.ThreadSafeEventDispatcher;
import ru.ntcap.desktop.initializers.InitPlaneInteractor;
import ru.ntcap.desktop.randomize.CyclicRankRandomizeInitPanel;
import ru.ntcap.desktop.randomize.HistogramRandomizeInitPanel;
import ru.ntcap.desktop.randomize.LifeRandomizeInitPanel;
import ru.ntcap.desktop.randomize.PhaseSensibleGasRandomizeInitPanel;
import ru.ntcap.desktop.recipes.ClosableJDialog;
import ru.ntcap.desktop.recipes.RecipeSelectPanel;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.io.File;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class App {
    private static JFrame frame;

    private static final int DEFAULT_SAFE_COLOR = 0x97E6F8;
    private static final double DEFAULT_HUE_SHIFT = 128.78;

    private static final File DEFAULT_COLOR_MAP_FILE = new File(App.class.getClassLoader().getResource("colorMaps").getPath());
    private static File colorMapFile = DEFAULT_COLOR_MAP_FILE;
    private static final ColorMap DEFAULT_COLOR_MAP = new SafeColorHashMap(Map.of(0, 0x000000), new HueShiftColorSupplier(DEFAULT_SAFE_COLOR, DEFAULT_HUE_SHIFT));
    private static ColorMap colorMap = DEFAULT_COLOR_MAP;

    private static CellularAutomatonLabel label;
    private static final Store store = new Store();
    private static final ThreadSafeEventDispatcher eventDispatcher = new ThreadSafeEventDispatcher();
    private static final ExecutorService eventDispatcherExecutor = Executors.newSingleThreadExecutor();

    public static void main(String... args) {
        final int WINDOW_WIDTH = 720;
        final int WINDOW_HEIGHT = 720;

        store.setDefaultPlaneWidth(WINDOW_WIDTH);
        store.setDefaultPlaneHeight(WINDOW_HEIGHT);

        frame = new JFrame("NTCAP");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        JMenuBar menuBar = createMenuBar();
        frame.setJMenuBar(menuBar);

        ViewportDisplayRule displayRule = new ViewportDisplayRule(colorMap);
        displayRule.setVelocity(10);

        var cellularAutomaton = new CyclicRank(WINDOW_WIDTH, WINDOW_HEIGHT, 16, 47.0);
        store.setCellularAutomaton(cellularAutomaton);
        store.setRecipeName("cyclic-rank");

        label = new CellularAutomatonLabel(cellularAutomaton, displayRule);
        label.setStepsPerSecond(20);

        JPanel panel = new JPanel(new BorderLayout());
        panel.setPreferredSize(new Dimension(WINDOW_WIDTH, WINDOW_HEIGHT));
        panel.setBorder(new EmptyBorder(10, 10, 10, 10));
        panel.add(label);

        ViewportKeyController controller = new ViewportKeyController(label, displayRule);
        label.addKeyListener(controller);

        eventDispatcher.addEventHandler("set-cellular-automaton", event -> {
            CellularAutomaton automaton = (CellularAutomaton)event.getParam("cellularAutomaton");
            String recipeName = (String)event.getParam("recipeName");

            CellularAutomaton old = store.getCellularAutomaton();
            for (int i = 0; i < Math.min(automaton.getPlanesCount(), old.getPlanesCount()); i += 1) {
                int[] plane = old.getPlane(i);
                automaton.initPlane(i, j -> plane[j]);
            }
            label.setSource(automaton);

            store.setCellularAutomaton(automaton);
            store.setRecipeName(recipeName);
        });

        frame.add(panel);
        frame.pack();
        frame.setSize(new Dimension(WINDOW_WIDTH, WINDOW_HEIGHT));
        frame.setLocationRelativeTo(null);

        frame.setVisible(true);

        InitPlaneInteractor initPlaneInteractor = new InitPlaneInteractor();
        eventDispatcher.addEventHandler("init-plane", initPlaneInteractor);

        eventDispatcher.enqueueEvent(new Event("init-plane", Map.of(
            "cellularAutomaton", cellularAutomaton,
            "recipeName", store.getRecipeName(),
            "patternName", "random",
            "probability", 10.0f
        )));

        eventDispatcherExecutor.execute(() -> {
            // 30 updates per second
            double DELTA_TIME = 1e9 / 30;
            long lastTime = 0;
            while (frame.isVisible()) {
                long nowTime = System.nanoTime();
                if (nowTime - lastTime >= DELTA_TIME) {
                    eventDispatcher.dispatch();
                    lastTime = nowTime;
                }
            }
        });
    }

    private static JMenuBar createMenuBar() {
        JMenuBar menuBar = new JMenuBar();

        JMenu fileMenu = new JMenu("File");
        JMenuItem openItem = new JMenuItem("Open...");
        JMenuItem saveItem = new JMenuItem("Save...");
        JMenuItem exitItem = new JMenuItem("Exit");
        fileMenu.add(openItem);
        fileMenu.add(saveItem);
        fileMenu.add(exitItem);

        JMenu ruleMenu = new JMenu("Rule");
        JMenuItem selectItem = new JMenuItem("Select...");
        selectItem.addActionListener(e -> showRecipeSelectDialog());
        JMenuItem randomizeItem = new JMenuItem("Randomize");
        JMenuItem saveRuleItem = new JMenuItem("Save...");
        JMenuItem loadRuleItem = new JMenuItem("Load...");
        ruleMenu.add(selectItem);
        ruleMenu.add(randomizeItem);
        ruleMenu.add(saveRuleItem);
        ruleMenu.add(loadRuleItem);

        JMenu displayRuleMenu = new JMenu("Display rule");
        JMenuItem loadDisplayRuleItem = new JMenuItem("Load...");
        displayRuleMenu.add(loadDisplayRuleItem);

        JMenu initializeMenu = new JMenu("Initialization");
        JMenuItem thisRecipeItem = new JMenuItem("This recipe...");
        JMenuItem randomMenuItem = new JMenuItem("Randomize...");
        randomMenuItem.addActionListener(e -> {
            ClosableJDialog dialog = new ClosableJDialog(frame, "Randomize", eventDispatcher);
            final Map<String, JPanel> ITEMS = Map.of(
                "life", new LifeRandomizeInitPanel(store, eventDispatcher),
                "histogram", new HistogramRandomizeInitPanel(store, eventDispatcher),
                "phase-sensible-gas", new PhaseSensibleGasRandomizeInitPanel(store, eventDispatcher),
                "cyclic-rank", new CyclicRankRandomizeInitPanel(store, eventDispatcher)
            );
            String recipeName = store.getRecipeName();
            // FIXME: show jdialog
            dialog.add(ITEMS.get(recipeName));
            dialog.pack();
            dialog.setVisible(true);
        });
        JMenuItem squareMenuItem = new JMenuItem("Square...");
        JMenuItem circleMenuItem = new JMenuItem("Circle...");
        initializeMenu.add(thisRecipeItem);
        initializeMenu.add(randomMenuItem);
        initializeMenu.add(squareMenuItem);
        initializeMenu.add(circleMenuItem);

        JMenu colorMapMenu = new JMenu("Color map");
        JMenuItem loadColorMapItem = new JMenuItem("Load...");
        loadColorMapItem.addActionListener(e -> {
            JFileChooser fc = new JFileChooser(colorMapFile);
            fc.setFileFilter(new FileNameExtensionFilter("Color map files", "cm"));
            int returnState = fc.showOpenDialog(frame);
            if (returnState == JFileChooser.APPROVE_OPTION) {
                File file = fc.getSelectedFile();
                colorMapFile = file.getParentFile();
                try {
                    colorMap = new SafeColorHashMap(ColorMapParser.parse(file.toPath()), new HueShiftColorSupplier(DEFAULT_SAFE_COLOR, DEFAULT_HUE_SHIFT));
                    label.setColorMap(colorMap);
                } catch (Exception ex) {
                    // TODO: add popup menu
                    ex.printStackTrace();
                }
            }
        });
        colorMapMenu.add(loadColorMapItem);

        menuBar.add(fileMenu);
        menuBar.add(ruleMenu);
        menuBar.add(displayRuleMenu);
        menuBar.add(initializeMenu);
        menuBar.add(colorMapMenu);

        return menuBar;
    }

    private static void showRecipeSelectDialog() {
        ClosableJDialog dialog = new ClosableJDialog(frame, "Select recipe", eventDispatcher);
        dialog.add(new RecipeSelectPanel(store, eventDispatcher));
        dialog.pack();
        dialog.setVisible(true);
    }
}
