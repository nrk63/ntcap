package ru.ntcap.desktop.filesystem;

public class FileSystemFactory {
    public static FileSystem getInstance() {
        if (custom != null) {
            return custom;
        }
        return new DefaultFileSystem();
    }

    public static void setInstance(FileSystem custom) {
        FileSystemFactory.custom = custom;
    }

    public static void reset() {
        custom = null;
    }

    private static FileSystem custom;
}
