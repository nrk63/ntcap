package ru.ntcap.desktop.filesystem;

import java.io.IOException;
import java.nio.file.Path;
import java.util.List;

public interface FileSystem {
    List<String> readLines(Path path) throws IOException;
}
