package ru.ntcap.desktop;

import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class LabeledTextField extends JPanel {
    public LabeledTextField(String label, int columns) {
        this(label, "", columns);
    }

    public LabeledTextField(String label, String defaultValue, int columns) {
        super();
        setLayout(new BoxLayout(this, BoxLayout.X_AXIS));

        add(new JLabel(label));
        textField = new JTextField(String.valueOf(defaultValue), columns);
        add(textField);
    }

    public JTextField getTextField() {
        return textField;
    }

    private final JTextField textField;
}
