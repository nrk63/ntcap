package ru.ntcap.desktop;

import ru.ntcap.core.CellularAutomaton;

public interface CellularAutomatonStore {
    CellularAutomaton getCellularAutomaton();
    String getRecipeName();
}
