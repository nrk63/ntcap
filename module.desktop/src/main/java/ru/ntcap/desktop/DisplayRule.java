/*NTCAP Copyright (C) 2021-2022  Georgij Krajnyukov <nrk63@yandex.ru>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package ru.ntcap.desktop;

import ru.ntcap.core.CellularAutomatonView;
import ru.ntcap.desktop.colormap.ColorMap;

import java.awt.Dimension;

/**
 * This class defines a method to translate cellular automaton data to colored pixels.
 * @author Georgij Krajnyukov
 * @version 0.1.0
 */
public interface DisplayRule {
    /**
     * Translates data from cellular automaton to 8-bit RGB pixels.
     * @param pixelsSize dimensions of parameter pixel area
     * @param rgbPixels square area of 8-bit RGB colors packed into integers
     * @param cellularAutomaton cellular automaton
     */
    void setRgbPixels(Dimension pixelsSize, int[] rgbPixels, CellularAutomatonView cellularAutomaton);

    void setColorMap(ColorMap colorMap);
}
