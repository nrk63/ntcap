package ru.ntcap.desktop.randomize;

import ru.ntcap.core.CellularAutomaton;
import ru.ntcap.desktop.CellularAutomatonStore;
import ru.ntcap.desktop.LabeledTextField;
import ru.ntcap.desktop.events.Event;
import ru.ntcap.desktop.events.EventDispatcher;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JPanel;
import java.util.Map;

public class LifeRandomizeInitPanel extends JPanel {
    public LifeRandomizeInitPanel(CellularAutomatonStore store, EventDispatcher eventDispatcher) {
        super();
        BoxLayout boxLayout = new BoxLayout(this, BoxLayout.Y_AXIS);
        setLayout(boxLayout);

        LabeledTextField probabilityPanel = new LabeledTextField("Probability", 6);
        JButton okButton = new JButton("Done");

        add(probabilityPanel);
        add(okButton);

        okButton.addActionListener(e -> {
            try {
                float probability = Float.parseFloat(probabilityPanel.getTextField().getText());

                CellularAutomaton cellularAutomaton = store.getCellularAutomaton();
                String recipeName = store.getRecipeName();

                eventDispatcher.enqueueEvent(new Event("init-plane", Map.of(
                    "cellularAutomaton", cellularAutomaton,
                    "recipeName", recipeName,
                    "patternName", "random",
                    "probability", probability
                )));
                eventDispatcher.enqueueEvent(new Event("close-dialog"));
            } catch (NumberFormatException ex) {
                ex.printStackTrace();
                // TODO: add JDialog
            }
        });
    }
}
