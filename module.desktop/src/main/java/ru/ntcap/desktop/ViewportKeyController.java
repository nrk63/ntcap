package ru.ntcap.desktop;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.HashSet;
import java.util.Set;

public class ViewportKeyController extends KeyAdapter {
    public ViewportKeyController(CellularAutomatonLabel label, ViewportDisplayRule displayRule) {
        if (displayRule != label.getDisplayRule()) {
            throw new IllegalArgumentException("display rule does not match the one set in the label");
        }

        this.label = label;
        this.displayRule = displayRule;
    }

    @Override
    public void keyPressed(KeyEvent e) {
        keyCodes.add(e.getKeyCode());
        update();
    }

    @Override
    public void keyReleased(KeyEvent e) {
        keyCodes.remove(e.getKeyCode());
    }

    protected void update() {
        if (keyCodes.contains(KeyEvent.VK_P)) {
            if (label.isRunning())
                label.stop();
            else
                label.start();
            keyCodes.remove(KeyEvent.VK_P);
        }

        if (keyCodes.contains(KeyEvent.VK_UP)) {
            displayRule.move(ViewportDisplayRule.Direction.UP);
        }

        if (keyCodes.contains(KeyEvent.VK_DOWN)) {
            displayRule.move(ViewportDisplayRule.Direction.DOWN);
        }

        if (keyCodes.contains(KeyEvent.VK_LEFT)) {
            displayRule.move(ViewportDisplayRule.Direction.LEFT);
        }

        if (keyCodes.contains(KeyEvent.VK_RIGHT)) {
            displayRule.move(ViewportDisplayRule.Direction.RIGHT);
        }

        if (keyCodes.contains(KeyEvent.VK_F)) {
            displayRule.setVelocity(displayRule.getVelocity()
                                            + DEFAULT_CAMERA_ACCELERATION);
        }

        if (keyCodes.contains(KeyEvent.VK_S)) {
            int resultVelocity = displayRule.getVelocity() - DEFAULT_CAMERA_ACCELERATION;
            if (resultVelocity < 0) {
                resultVelocity = MIN_CAMERA_SPEED;
            }
            displayRule.setVelocity(resultVelocity);
        }

        if (keyCodes.contains(KeyEvent.VK_B)) {
            displayRule.zoomIn(1);
        }

        if (keyCodes.contains(KeyEvent.VK_L)) {
            displayRule.zoomOut(1);
        }

        label.repaint();
    }

    protected boolean isKeyPressed(int keyCode) {
        return keyCodes.contains(keyCode);
    }

    private final Set<Integer> keyCodes = new HashSet<>();
    private final CellularAutomatonLabel label;
    private final ViewportDisplayRule displayRule;

    private static final int DEFAULT_CAMERA_ACCELERATION = 1;
    private static final int MIN_CAMERA_SPEED = 1;
}
