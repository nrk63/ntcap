/*NTCAP Copyright (C) 2021-2022  Georgij Krajnyukov <nrk63@yandex.ru>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package ru.ntcap.desktop;

import ru.ntcap.core.CellularAutomatonView;
import ru.ntcap.desktop.colormap.ColorMap;

import java.awt.Dimension;
import java.awt.Point;
import java.awt.Rectangle;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

/**
 * This implementation of {@link DisplayRule} interface provides movable and scalable
 * viewport and supports color maps. This class is thread-safe.
 * @author Georgij Krajnyukov
 * @version 0.3.0
 */
public class ViewportDisplayRule implements DisplayRule {
    /**
     * Creates instance with parameter position, velocity, scale and color map.
     * @param position initial position of viewport
     * @param velocity initial velocity of viewport
     * @param pixelsPerCell initial number of pixels per cell
     * @param colorMap color map where key is cell state, value is 8-bit RGB color
     *
     * @throws NullPointerException if position or color map is {@code null}
     */
    public ViewportDisplayRule(Point position, int velocity, int pixelsPerCell,
                               ColorMap colorMap) {
        Objects.requireNonNull(position);
        Objects.requireNonNull(colorMap);
        this.position = new AtomicReference<>(position);
        this.velocity = new AtomicInteger(velocity);
        this.pixelsPerCell = new AtomicInteger(pixelsPerCell);
        this.colorMap = colorMap;
    }

    /**
     * Creates instance with (0, 0) position, 1:1 scale and specified color map.
     * @param colorMap color map where key is cell state, value is 8-bit RGB color
     *
     * @throws NullPointerException if color map is {@code null}
     */
    public ViewportDisplayRule(ColorMap colorMap) {
        this(new Point(0, 0), 1, 1, colorMap);
    }

    /**
     * Returns a copy of this instance with altered color map.
     * @param colorMap color map where key is cell state, value is 8-bit RGB color
     * @return instance with the same viewport state, but specified color map
     */
    public ViewportDisplayRule withColorMap(ColorMap colorMap) {
        return new ViewportDisplayRule(getPosition(), getVelocity(), getPixelsPerCell(), colorMap);
    }

    /**
     * Translates data from cellular automaton to 8-bit RGB pixels basing on this instance
     * viewport's state and color map.
     * @implSpec
     * This method uses {@link #extractData(CellularAutomatonView)} to form the data array.
     * Cells shape could be modified by overriding
     * {@link #drawCell(Dimension, int[], Point, int, int)} method.
     *
     * @param pixelsSize dimensions of parameter pixel area
     * @param rgbPixels area of 8-bit RGB colors packed into integers
     * @param view cellular automaton
     */
    public void setRgbPixels(Dimension pixelsSize, int[] rgbPixels,
                             CellularAutomatonView view) {
        Dimension dataSize = new Dimension(view.getPlaneWidth(),
                                           view.getPlaneHeight());
        Point adjustedPosition = adjustAndGetPosition(dataSize);
        int fixedCellSize = pixelsPerCell.get();
        int[] data = extractData(view);

        int pixelsCellsWidth = (int)Math.ceil(pixelsSize.width / (double)fixedCellSize);
        int pixelsCellsHeight = (int)Math.ceil(pixelsSize.height / (double)fixedCellSize);

        Rectangle fittingArea = new Rectangle(adjustedPosition.x,
                                              adjustedPosition.y,
                                             dataSize.width - adjustedPosition.x,
                                             dataSize.height - adjustedPosition.y);
        fittingArea.width = Math.min(fittingArea.width, pixelsCellsWidth);
        fittingArea.height = Math.min(fittingArea.height, pixelsCellsHeight);

        Rectangle bottomArea = new Rectangle(fittingArea.x,
                                             0,
                                             fittingArea.width,
                                             pixelsCellsHeight - fittingArea.height);
        Rectangle rightArea = new Rectangle(0,
                                            fittingArea.y,
                                            pixelsCellsWidth - fittingArea.width,
                                            fittingArea.height);
        Rectangle cornerArea = new Rectangle(0, 0, rightArea.width, bottomArea.height);

        Point fittingAreaOrigin = new Point(0, 0);
        Point bottomAreaOrigin = new Point(0, fittingArea.height*fixedCellSize);
        Point rightAreaOrigin = new Point(fittingArea.width*fixedCellSize, 0);
        Point cornerAreaOrigin = new Point(rightAreaOrigin.x, bottomAreaOrigin.y);

        drawArea(pixelsSize, rgbPixels, dataSize, data, fittingAreaOrigin, fittingArea, fixedCellSize);
        drawArea(pixelsSize, rgbPixels, dataSize, data, bottomAreaOrigin, bottomArea, fixedCellSize);
        drawArea(pixelsSize, rgbPixels, dataSize, data, rightAreaOrigin, rightArea, fixedCellSize);
        drawArea(pixelsSize, rgbPixels, dataSize, data, cornerAreaOrigin, cornerArea, fixedCellSize);
    }

    /**
     * Directions to move viewport.
     * @author Georgij Krajnyukov
     * @version 0.1.0
     */
    public enum Direction {
        /**
         * direction of y coordinate decreasing
         */
        UP,

        /**
         * direction of x coordinate decreasing
         */
        LEFT,

        /**
         * direction of y coordinate increasing
         */
        DOWN,

        /**
         * direction of x coordinate increasing
         */
        RIGHT,
    }

    /**
     * Moves the viewport in specified direction using this instance viewport velocity.
     * @param direction direction to move viewport
     */
    public void move(Direction direction) {
        Point newPosition = new Point(position.get());
        int fixedVelocity = velocity.get();
        switch (direction) {
            case UP -> newPosition.y -= fixedVelocity;
            case LEFT -> newPosition.x -= fixedVelocity;
            case DOWN -> newPosition.y += fixedVelocity;
            case RIGHT -> newPosition.x += fixedVelocity;
        }
        position.set(newPosition);
    }

    /**
     * Increases scale by 2 specified number of times.
     * @param repetitionCount number of zoom in repetitions
     *
     * @throws IllegalArgumentException if repetition count is negative
     */
    public void zoomIn(int repetitionCount) {
        if (repetitionCount < 0)
            throw new IllegalArgumentException("negative repetition count: " + repetitionCount);
        int value = pixelsPerCell.get();
        value *= (int) Math.pow(2, repetitionCount);
        pixelsPerCell.set(value);
    }

    /**
     * Decreases scale by 2 specified number of times. If scale is 1:1, does nothing.
     * @param repetitionCount number of zoom out repetitions
     *
     * @throws IllegalArgumentException if repetition count is negative
     */
    public void zoomOut(int repetitionCount) {
        if (repetitionCount < 0)
            throw new IllegalArgumentException("negative repetition count: " + repetitionCount);
        int newCellSize = pixelsPerCell.get();
        if (newCellSize == 1)
            return;
        newCellSize /= (int) Math.pow(2, repetitionCount);
        pixelsPerCell.set(newCellSize);
    }

    /**
     * Returns current viewport's position. Coordinates could have any values, even
     * negative.
     * @return current viewport's position
     */
    public Point getPosition() {
        return position.get();
    }

    /**
     * Gets number of pixels per cell representing viewport's scale.
     * @return number of pixels per cell
     */
    public int getPixelsPerCell() {
        return pixelsPerCell.get();
    }

    /**
     * Gets viewport's velocity measured in cells per movement.
     * @return viewport's velocity measured in cells per movement
     */
    public int getVelocity() {
        return velocity.get();
    }

    /**
     * Sets viewport velocity.
     * @param velocity viewport velocity measured in cells per movement
     * @throws IllegalArgumentException if velocity is negative
     */
    public void setVelocity(int velocity) {
        if (velocity < 0)
            throw new IllegalArgumentException("negative velocity: " + velocity);
        this.velocity.set(velocity);
    }

    @Override
    public void setColorMap(ColorMap colorMap) {
        this.colorMap = colorMap;
    }

    /**
     * Adjusts this instance viewport position to parameter bounds and returns a copy.
     * @param bounds bounds to which values are adjusted
     * @return copy of adjusted position
     */
    protected Point adjustAndGetPosition(Dimension bounds) {
        Point adjustedPosition = new Point(position.get());
        adjustedPosition.x = (adjustedPosition.x + bounds.width) % bounds.width;
        adjustedPosition.y = (adjustedPosition.y + bounds.height) % bounds.height;
        position.set(adjustedPosition);
        return adjustedPosition;
    }

    /**
     * Returns array of states formed by given cellular automaton. The result may be
     * an argument of methods translating cells to pixels using this instance color map,
     * so these states should be the valid keys.
     * @param view cellular automaton to extract data from
     * @return array of states formed by given cellular automaton
     */
    protected int[] extractData(CellularAutomatonView view) {
        return view.getPlane(0);
    }

    /**
     * Gets color of parameter state from this instance color map.
     * @param state state of cell
     * @return 8-bit RGB color packed into integer
     * @throws NullPointerException if parameter state is not a key of color map
     */
    protected int getColor(int state) {
        return colorMap.getRgbOf(state);
    }

    /**
     * Draws a cell of specified position, size and color on pixel area.
     * @param pixelsSize dimensions of parameter pixel area
     * @param rgbPixels area of 8-bit RGB colors packed into integers
     * @param upperLeft location of cell's upper left corner
     * @param cellLength cell's side length
     * @param color color of cell
     */
    protected void drawCell(Dimension pixelsSize, int[] rgbPixels,
                            Point upperLeft, int cellLength, int color) {
        int rightBorder = Math.min(upperLeft.x + cellLength, pixelsSize.width);
        int bottomBorder = Math.min(upperLeft.y + cellLength, pixelsSize.height);
        for (int y = upperLeft.y; y < bottomBorder; ++y) {
            for (int x = upperLeft.x; x < rightBorder; ++x) {
                rgbPixels[x + y*pixelsSize.width] = color;
            }
        }
    }

    /**
     * Draws cellular area, whose scaled dimensions do not exceed pixels area size.
     * @param pixelsSize dimensions of parameter pixel area
     * @param rgbPixels area of 8-bit RGB colors packed into integers
     * @param dataSize dimensions of parameter cellular area
     * @param data array of states valid for this instance color map
     * @param origin point of pixel area to start drawing from
     * @param areaSize dimensions of area measured in cells
     * @param pixelsPerCell number of pixels per cell
     */
    private void drawArea(Dimension pixelsSize, int[] rgbPixels,
                          Dimension dataSize, int[] data,
                          Point origin, Rectangle areaSize, int pixelsPerCell) {
        Point cellUpperLeft = new Point(origin);
        for (int y = 0; y < areaSize.height; ++y) {
            for (int x = 0; x < areaSize.width; ++x) {
                int state = data[(areaSize.x + x) + (areaSize.y + y)*dataSize.width];
                int color = colorMap.getRgbOf(state);
                drawCell(pixelsSize, rgbPixels, cellUpperLeft, pixelsPerCell, color);
                cellUpperLeft.x += pixelsPerCell;
            }
            cellUpperLeft.x = origin.x;
            cellUpperLeft.y += pixelsPerCell;
        }
    }

    /**
     * upper left corner of viewport measured in cells
     */
    private final AtomicReference<Point> position;

    /**
     * velocity of viewport measured in cells per movement
     */
    private final AtomicInteger velocity;

    /**
     * number of pixels per cell
     */
    private final AtomicInteger pixelsPerCell;

    /**
     * color map where key is cell state, value is 8-bit RGB color
     */
    private ColorMap colorMap;
}
