package ru.ntcap.desktop.colormap;

import ru.ntcap.desktop.filesystem.FileSystem;
import ru.ntcap.desktop.filesystem.FileSystemFactory;

import java.io.IOException;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ColorMapParser {
    public static Map<Integer, Integer> parse(Path path) throws IOException, ColorFormatException {
        FileSystem fs = FileSystemFactory.getInstance();
        Map<Integer, Integer> resultMap = new HashMap<>();
        for (String line : fs.readLines(path)) {
            if (line.isEmpty()) {
                continue;
            }

            Matcher matcher = LINE_PATTERN.matcher(line);
            if (!matcher.find()) {
                throw ColorFormatException.ofLine(line);
            }

            String stateString = matcher.group(1);
            String colorString = matcher.group(2).substring(2);

            try {
                int state = Integer.parseInt(stateString);
                int color = Integer.parseInt(colorString, 16);
                resultMap.put(state, color);
            } catch (NumberFormatException e) {
                throw ColorFormatException.ofLine(line, e);
            }
        }

        return resultMap;
    }

    private static final Pattern LINE_PATTERN = Pattern.compile("(\\d+)\\s->\\s(\\d*x*[\\dA-F]+)");
}
