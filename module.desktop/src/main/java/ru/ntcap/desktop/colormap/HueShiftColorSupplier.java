package ru.ntcap.desktop.colormap;

public class HueShiftColorSupplier extends HueColorsSupplier {
    public HueShiftColorSupplier(int initColor, double shift) {
        super(initColor, color -> {
            double newHue = (color.hue() + shift) % 360;
            return new HsvColor(newHue, color.saturation(), color.value());
        });
    }
}
