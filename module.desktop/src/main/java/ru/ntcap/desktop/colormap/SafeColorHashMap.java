package ru.ntcap.desktop.colormap;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Supplier;

public class SafeColorHashMap implements ColorMap {
    public SafeColorHashMap(Map<Integer, Integer> initColors, Supplier<Integer> colorsSupplier) {
        this.colors = new HashMap<>(initColors);
        this.colorsSupplier = colorsSupplier;
    }

    @Override
    public int getRgbOf(int state) {
        if (colors.containsKey(state)) {
            return colors.get(state);
        }
        colors.put(state, colorsSupplier.get());
        return colors.get(state);
    }

    private final Map<Integer, Integer> colors;
    private final Supplier<Integer> colorsSupplier;
}
