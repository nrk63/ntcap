package ru.ntcap.desktop.colormap;

public class ColorFormatException extends Exception {
    public static ColorFormatException ofLine(String line) {
        return new ColorFormatException(line);
    }

    public static ColorFormatException ofLine(String line, Throwable e) {
        return new ColorFormatException(line, e);
    }

    private ColorFormatException(String line) {
        super("Invalid format: " + line);
    }

    private ColorFormatException(String line, Throwable e) {
        super("Invalid format: " + line, e);
    }
}
