package ru.ntcap.desktop.colormap;

import java.util.function.Supplier;

public class HueColorsSupplier implements Supplier<Integer> {
    public HueColorsSupplier(int initColor, HsvTransformer hsvTransformer) {
        this.nextColor = initColor;
        this.hsvTransformer = hsvTransformer;
    }

    @Override
    public Integer get() {
        int result = nextColor;
        calcNextColor();
        return result;
    }

    private void calcNextColor() {
        int red =   (nextColor & 0xFF0000) >> 16;
        int green = (nextColor & 0x00FF00) >> 8;
        int blue =   nextColor & 0x0000FF;

        double min = Math.min(Math.min(red, green), blue);
        double max = Math.max(Math.max(red, green), blue);
        if (min == max) {
            nextColor = 0;
            return;
        }

        double hue;
        if (max == red && green >= blue) {
            hue = 60*(green - blue)/(max - min);
        } else if (max == red && green < blue) {
            hue = 60*(green - blue)/(max - min) + 360;
        } else if (max == green) {
            hue = 60*(blue - red)/(max - min) + 120;
        } else {
            hue = 60*(red - green)/(max - min) + 240;
        }

        double saturation = 0.0;
        if (max != 0) {
            saturation = (1 - min/max) * 100.0;
        }

        double value = max/255 * 100;

        HsvColor currentColor = new HsvColor(hue, saturation, value);
        HsvColor newColor = hsvTransformer.transform(currentColor);
        hue = newColor.hue();
        saturation = newColor.saturation();
        value = newColor.value();

        double minValue = (100.0 - saturation)*value / 100.0;
        double parameter = (value - minValue) * (hue % 60) / 60.0;
        double incValue = minValue + parameter;
        double decValue = value - parameter;

        int discreteValue = (int) Math.round(value * 2.55);
        int discreteMinValue = (int) Math.round(minValue * 2.55);
        int discreteIncValue = (int) Math.round(incValue * 2.55);
        int discreteDecValue = (int) Math.round(decValue * 2.55);

        int region = (int)(hue / 60) % 6;
        switch (region) {
            case 0 -> nextColor = (discreteValue << 16) | (discreteIncValue << 8) | discreteMinValue;
            case 1 -> nextColor = (discreteDecValue << 16) | (discreteValue << 8) | discreteMinValue;
            case 2 -> nextColor = (discreteMinValue << 16) | (discreteValue << 8) | discreteIncValue;
            case 3 -> nextColor = (discreteMinValue << 16) | (discreteDecValue << 8) | discreteValue;
            case 4 -> nextColor = (discreteIncValue << 16) | (discreteMinValue << 8) | discreteValue;
            case 5 -> nextColor = (discreteValue << 16) | (discreteMinValue << 8) | discreteDecValue;
        }
    }

    private int nextColor = 0xFFFFFF;
    private final HsvTransformer hsvTransformer;
}
