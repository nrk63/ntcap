package ru.ntcap.desktop.colormap;

/**
 * Color represented in HSV color model.
 * @param hue hue in range [0.0; 360.0]
 * @param saturation saturation in range [0.0; 100.0]
 * @param value value in range [0.0; 100.0]
 * @author Georgij Krajnyukov
 * @version 0.1.0
 */
public record HsvColor(double hue, double saturation, double value) {}
