package ru.ntcap.desktop.colormap;

/**
 * This interface defines object that maps states into colors.
 * @author Georgij Krajnyukov
 * @version 0.1.0
 */
public interface ColorMap {
    /**
     * Gets rgb color of specified state.
     * @param state state which color is needed
     * @return rgb encoded int of format: 0xXXRRGGBB
     */
    int getRgbOf(int state);
}
