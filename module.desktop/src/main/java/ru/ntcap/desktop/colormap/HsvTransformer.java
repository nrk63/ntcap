package ru.ntcap.desktop.colormap;

public interface HsvTransformer {
    HsvColor transform(HsvColor color);
}
