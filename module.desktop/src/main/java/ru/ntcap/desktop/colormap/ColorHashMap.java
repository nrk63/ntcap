package ru.ntcap.desktop.colormap;

import java.util.HashMap;
import java.util.Map;
import java.util.NoSuchElementException;

/**
 * {@link ColorMap} implemented with a {@link java.util.HashMap}
 * @author Georgij Krajnyukov
 * @version 0.1.0
 */
public class ColorHashMap implements ColorMap{
    /**
     * Creates new instance with specified mapping. Keys are states,
     * values are 8-bit RGB encoded colors (0xXXRRGGBB)
     * @param colors map with states as a key and rgb colors as values
     */
    public ColorHashMap(Map<Integer, Integer> colors) {
        this.colors = new HashMap<>(colors);
    }

    /**
     * Gets rgb of specified state
     * @param state state which color is needed
     * @return rgb color of specified state
     * @throws NoSuchElementException if there is no color for specified state
     */
    @Override
    public int getRgbOf(int state) {
        if (colors.containsKey(state)) {
            return colors.get(state);
        }

        throw new NoSuchElementException("no color for state: " + state);
    }

    private final HashMap<Integer, Integer> colors;
}
