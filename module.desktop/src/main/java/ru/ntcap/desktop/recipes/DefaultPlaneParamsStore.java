package ru.ntcap.desktop.recipes;

public interface DefaultPlaneParamsStore {
    int getDefaultPlaneWidth();
    int getDefaultPlaneHeight();
}
