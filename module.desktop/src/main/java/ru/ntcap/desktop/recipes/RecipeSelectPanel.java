package ru.ntcap.desktop.recipes;

import ru.ntcap.desktop.events.EventDispatcher;

import javax.swing.BoxLayout;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import java.awt.CardLayout;

public class RecipeSelectPanel extends JPanel {
    public RecipeSelectPanel(DefaultPlaneParamsStore store, EventDispatcher eventDispatcher) {
        super();
        BoxLayout boxLayout = new BoxLayout(this, BoxLayout.Y_AXIS);
        setLayout(boxLayout);

        JPanel settingsPanel = new JPanel(new CardLayout());
        settingsPanel.add(new LifeSelectPanel(store, eventDispatcher), "life");
        settingsPanel.add(new HistogramSelectPanel(store, eventDispatcher), "histogram");
        settingsPanel.add(new PhaseSensibleGasSelectPanel(store, eventDispatcher), "phase-sensible-gas");
        settingsPanel.add(new CyclicRankSelectPanel(store, eventDispatcher), "cyclic-rank");

        JComboBox<String> recipesBox = new JComboBox<>(ITEMS);
        recipesBox.addActionListener(e -> {
            String selectedItem = (String)recipesBox.getSelectedItem();
            CardLayout cardLayout = (CardLayout)(settingsPanel.getLayout());
            cardLayout.show(settingsPanel, selectedItem);
        });

        add(recipesBox);
        add(settingsPanel);
    }

    private static final String[] ITEMS = new String[] {
        "life", "histogram", "phase-sensible-gas", "cyclic-rank"
    };
}
