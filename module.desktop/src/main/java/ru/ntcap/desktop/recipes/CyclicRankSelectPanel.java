package ru.ntcap.desktop.recipes;

import ru.ntcap.core.recipes.CyclicRank;
import ru.ntcap.desktop.LabeledTextField;
import ru.ntcap.desktop.events.Event;
import ru.ntcap.desktop.events.EventDispatcher;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JPanel;
import java.util.Map;

public class CyclicRankSelectPanel extends JPanel {
        public CyclicRankSelectPanel(DefaultPlaneParamsStore store, EventDispatcher eventDispatcher) {
        super();
        BoxLayout boxLayout = new BoxLayout(this, BoxLayout.Y_AXIS);
        setLayout(boxLayout);

        LabeledTextField widthPanel = new LabeledTextField("Width", String.valueOf(store.getDefaultPlaneWidth()), 6);
        LabeledTextField heightPanel = new LabeledTextField("Height", String.valueOf(store.getDefaultPlaneHeight()), 6);
        LabeledTextField statesCountPanel = new LabeledTextField("States count", String.valueOf(DEFAULT_STATES_COUNT), 6);
        LabeledTextField probabilityPanel = new LabeledTextField("Probability", 6);
        JButton okButton = new JButton("Done");

        add(widthPanel);
        add(heightPanel);
        add(statesCountPanel);
        add(probabilityPanel);
        add(okButton);

        okButton.addActionListener(e -> {
            try {
                int width = Integer.parseInt(widthPanel.getTextField().getText());
                int height = Integer.parseInt(heightPanel.getTextField().getText());
                int statesCount = Integer.parseInt(statesCountPanel.getTextField().getText());
                float probability = Float.parseFloat(probabilityPanel.getTextField().getText());

                CyclicRank cyclicRank = new CyclicRank(width, height, statesCount, probability);

                eventDispatcher.enqueueEvent(new Event("set-cellular-automaton", Map.of(
                    "cellularAutomaton", cyclicRank,
                    "recipeName", "cyclic-rank"
                )));
                eventDispatcher.enqueueEvent(new Event("close-dialog"));
            } catch (NumberFormatException ex) {
                ex.printStackTrace();
                // TODO: add JDialog
            }
        });
    }

    private static final int DEFAULT_STATES_COUNT = 4;
}
