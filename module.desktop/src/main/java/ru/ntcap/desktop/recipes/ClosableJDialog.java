package ru.ntcap.desktop.recipes;

import ru.ntcap.desktop.events.Event;
import ru.ntcap.desktop.events.EventDispatcher;
import ru.ntcap.desktop.events.EventHandler;

import javax.swing.JDialog;
import javax.swing.JFrame;
import java.util.Map;

public class ClosableJDialog extends JDialog implements EventHandler {
    public ClosableJDialog(JFrame frame, String title, EventDispatcher eventDispatcher) {
        super(frame, title, true);
        setLocationRelativeTo(frame);

        this.eventDispatcher = eventDispatcher;
        eventDispatcher.addEventHandler("close-dialog", this);
    }

    @Override
    public void handleEvent(Event event) {
        if (!event.getName().equals("close-dialog"))
            return;

        setVisible(false);
        eventDispatcher.enqueueEvent(new Event("remove-event-handler",
            Map.of("eventName", "close-dialog",
                   "eventHandler", this)
        ));
    }

    private final EventDispatcher eventDispatcher;
}
