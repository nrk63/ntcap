package ru.ntcap.desktop.recipes;

import ru.ntcap.core.recipes.Life;
import ru.ntcap.desktop.LabeledTextField;
import ru.ntcap.desktop.events.Event;
import ru.ntcap.desktop.events.EventDispatcher;

import javax.swing.*;
import java.util.Map;

public class LifeSelectPanel extends JPanel {
    public LifeSelectPanel(DefaultPlaneParamsStore store, EventDispatcher eventDispatcher) {
        super();
        BoxLayout boxLayout = new BoxLayout(this, BoxLayout.Y_AXIS);
        setLayout(boxLayout);

        LabeledTextField widthPanel = new LabeledTextField("Width", String.valueOf(store.getDefaultPlaneWidth()), 6);
        LabeledTextField heightPanel = new LabeledTextField("Height", String.valueOf(store.getDefaultPlaneHeight()), 6);
        JButton okButton = new JButton("Done");

        add(widthPanel);
        add(heightPanel);
        add(okButton);

        okButton.addActionListener(e -> {
            try {
                int width = Integer.parseInt(widthPanel.getTextField().getText());
                int height = Integer.parseInt(heightPanel.getTextField().getText());

                Life life = new Life(width, height);

                Event event = new Event("set-cellular-automaton", Map.of(
                    "cellularAutomaton", life,
                        "recipeName", "life"
                ));
                eventDispatcher.enqueueEvent(event);

                event = new Event("close-dialog");
                eventDispatcher.enqueueEvent(event);
            } catch (NumberFormatException ex) {
                ex.printStackTrace();
                // TODO: add JDialog
            }
        });
    }
}
