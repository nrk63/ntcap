package ru.ntcap.desktop.events;

import java.util.Map;
import java.util.NoSuchElementException;

/**
 * Event with arbitrary name and set of parameters. Each parameter is unique.
 * @author Georgij Krajnyukov
 * @version 0.1.0
 */
public class Event {
    /**
     * Creates new instance with specified name, without parameters.
     * @param name name of this event
     */
    public Event(String name) {
        this(name, Map.of());
    }

    /**
     * Creates new instance with specified name and parameters.
     * @param name name of this event
     * @param params parameters of this event
     */
    public Event(String name, Map<String, Object> params) {
        this.name = name;
        this.params = Map.copyOf(params);
    }

    /**
     * Gets this event name.
     * @return name of this event
     */
    public String getName() {
        return name;
    }

    /**
     * Returns {@code true} if specified name match this instance name.
     * Literally does {@code event.getName().equals(name)}.
     * @param name event name to compare with
     * @return true if this event's name and specified name match
     */
    public boolean hasName(String name) {
        return getName().equals(name);
    }

    /**
     * Gets parameter with specified name. Its type is considered
     * to be known by both sender and receiver.
     * @param name name of parameter
     * @return parameter object
     * @throws NoSuchElementException if this event contains no parameter with specified name
     */
    public Object getParam(String name) {
        if (params.containsKey(name)) {
            return params.get(name);
        }

        throw new NoSuchElementException("no param: " + name);
    }

    @Override
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }

        if (other == null || getClass() != other.getClass()) {
            return false;
        }

        Event event = (Event) other;

        if (!getName().equals(event.getName())) {
            return false;
        }

        return params.equals(event.params);
    }

    @Override
    public int hashCode() {
        return 31 * getName().hashCode() + params.hashCode();
    }

    @Override
    public String toString() {
        return "Event[" + name + "]";
    }

    private final String name;
    private final Map<String, Object> params;
}
