package ru.ntcap.desktop.events;

/**
 * Entity that handles event.
 * @author Georgij Krajnyukov
 * @version 0.1.0
 */
public interface EventHandler {
    /**
     * Handles the event in any way.
     * @param event event to handle
     */
    void handleEvent(Event event);
}
