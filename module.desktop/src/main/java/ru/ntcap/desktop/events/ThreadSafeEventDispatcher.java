package ru.ntcap.desktop.events;

import com.google.common.collect.Multimap;

import java.util.Queue;

/**
 * Humble object for {@link SingleThreadEventDispatcher} which is thread safe.
 * Also implements {@link EventHandler} in order to permit event handlers
 * to unbind it by itself.
 * @author Georgij Krajnyukov
 * @version 0.1.0
 */
public class ThreadSafeEventDispatcher implements EventDispatcher, EventHandler {
    @Override
    public void addEventHandler(String eventName, EventHandler eventHandler) {
        synchronized (eventHandlersMutex) {
            eventDispatcher.addEventHandler(eventName, eventHandler);
        }
    }

    @Override
    public void enqueueEvent(Event event) {
        synchronized (queueMutex) {
            eventDispatcher.enqueueEvent(event);
        }
    }

    public void dispatch() {
        Multimap<String, EventHandler> handlers;
        synchronized (eventHandlersMutex) {
            handlers = eventDispatcher.copyEventHandlers();
        }

        Queue<Event> events;
        synchronized (queueMutex) {
            events = eventDispatcher.copyAndClearEventQueue();
        }

        SingleThreadEventDispatcher.dispatch(events, handlers);
    }

    @Override
    public void handleEvent(Event event) {
        if (!event.getName().equals("remove-event-handler"))
            return;

        String eventName = (String)event.getParam("eventName");
        EventHandler eventHandler = (EventHandler)event.getParam("eventHandler");

        synchronized (eventHandlersMutex) {
            eventDispatcher.removeEventHandler(eventName, eventHandler);
        }
    }

    private final Object queueMutex = new Object();
    private final Object eventHandlersMutex = new Object();
    private final SingleThreadEventDispatcher eventDispatcher = new SingleThreadEventDispatcher();
}
