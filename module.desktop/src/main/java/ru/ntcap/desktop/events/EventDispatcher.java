package ru.ntcap.desktop.events;

/**
 * Unit responsible for enqueueing events and adding new event handlers.
 * @author Georgij Krajnyukov
 * @version 0.1.0
 */
public interface EventDispatcher {
    /**
     * Adds given event handlers so that it will receive events with
     * specified name.
     * @param eventName name of event
     * @param eventHandler event handler
     */
    void addEventHandler(String eventName, EventHandler eventHandler);

    /**
     * Enqueues the event.
     * @param event event to enqueue
     */
    void enqueueEvent(Event event);
}
