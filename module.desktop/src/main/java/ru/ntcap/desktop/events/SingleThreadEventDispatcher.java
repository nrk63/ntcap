package ru.ntcap.desktop.events;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;

import java.util.ArrayDeque;
import java.util.Queue;

/**
 * Single thread class needed to implement testable thread safe
 * {@link EventDispatcher}. This implementation has extractable event queue
 * and event handlers multimap.
 * @author Georgij Krajnyukov
 * @version 0.1.0
 */
public class SingleThreadEventDispatcher {
    /**
     * Adds event handler. Should be called with {@code eventHandlers} mutex locked.
     * @param eventName name of event
     * @param eventHandler event handler instance
     */
    public void addEventHandler(String eventName, EventHandler eventHandler) {
        eventHandlers.put(eventName, eventHandler);
    }

    /**
     * Removes event handler. Should be called with {@code eventHandlers} mutex locked.
     * @param eventName name of event
     * @param eventHandler event handler instance
     */
    public void removeEventHandler(String eventName, EventHandler eventHandler) {
        eventHandlers.remove(eventName, eventHandler);
    }

    /**
     * Enqueues the event. Should be called with {@code eventQueue} mutex locked.
     * @param event event to enqueue
     */
    public void enqueueEvent(Event event) {
        eventQueue.add(event);
    }

    /**
     * Returns current event queue copy and clears the original one.
     * Should be called with {@code eventQueue} mutex locked.
     * @return copy of event queue before clearing
     */
    public Queue<Event> copyAndClearEventQueue() {
        Queue<Event> copy = new ArrayDeque<>(eventQueue);
        eventQueue.clear();
        return copy;
    }

    /**
     * Returns event handlers multimap copy where key is event name, value is event handler.
     * Should be called with {@code eventHandlers} mutex locked.
     * @return copy of event handlers multimap where key is event name, value is event handler
     */
    public Multimap<String, EventHandler> copyEventHandlers() {
        return ArrayListMultimap.create(eventHandlers);
    }

    /**
     * For every event calls {@link EventHandler#handleEvent} of every event handler which
     * is mapped to the same event name
     * @param events event queues to dispatch
     * @param eventHandlers multimap where key is event name, value is event handler
     */
    public static void dispatch(Queue<Event> events, Multimap<String, EventHandler> eventHandlers) {
        for (Event event : events) {
            for (EventHandler handler : eventHandlers.get(event.getName())) {
                handler.handleEvent(event);
            }
        }
    }

    private final Queue<Event> eventQueue = new ArrayDeque<>();
    private final Multimap<String, EventHandler> eventHandlers = ArrayListMultimap.create();
}
