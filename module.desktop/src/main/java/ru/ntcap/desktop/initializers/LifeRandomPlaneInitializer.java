package ru.ntcap.desktop.initializers;

import ru.ntcap.core.recipes.Life;
import ru.ntcap.desktop.events.Event;

import java.util.Random;

public class LifeRandomPlaneInitializer implements PlaneInitializer {
    @Override
    public void initialize(Event event) {
        Life life = (Life) event.getParam("cellularAutomaton");
        Random random = new Random();
        float probability = (Float)event.getParam("probability");
        life.initPlane(0, i -> {
            if (random.nextFloat() * 100.0f <= probability) {
                return 1;
            }
            return 0;
        });
    }
}
