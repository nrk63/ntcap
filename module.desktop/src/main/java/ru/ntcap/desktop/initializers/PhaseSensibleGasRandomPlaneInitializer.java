package ru.ntcap.desktop.initializers;

import ru.ntcap.core.recipes.PhaseSensibleGas;
import ru.ntcap.desktop.events.Event;

import java.util.Random;

public class PhaseSensibleGasRandomPlaneInitializer implements PlaneInitializer {
    @Override
    public void initialize(Event event) {
        PhaseSensibleGas gas = (PhaseSensibleGas) event.getParam("cellularAutomaton");
        float probability = (Float)event.getParam("probability");
        int statesCount = (Integer) event.getParam("statesCount");

        Random random = new Random();
        gas.initPlane(0, i -> {
            if (random.nextFloat() * 100.0f <= probability) {
                return random.nextInt(statesCount - 1) + 1;
            }
            return 0;
        });
    }
}
