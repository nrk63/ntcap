package ru.ntcap.desktop.initializers;

import ru.ntcap.core.recipes.CyclicRank;
import ru.ntcap.desktop.events.Event;

import java.util.Random;

public class CyclicRankRandomPlaneInitializer implements PlaneInitializer {
    @Override
    public void initialize(Event event) {
        CyclicRank cyclicRank = (CyclicRank) event.getParam("cellularAutomaton");
        int statesCount = cyclicRank.getStatesCount();
        float probability = (Float)event.getParam("probability");

        Random random = new Random();
        cyclicRank.initPlane(0, i -> {
            if (random.nextFloat() * 100.0f <= probability) {
                return random.nextInt(statesCount - 1) + 1;
            }
            return 0;
        });
    }
}
