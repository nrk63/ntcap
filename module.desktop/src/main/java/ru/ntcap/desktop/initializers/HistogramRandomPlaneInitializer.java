package ru.ntcap.desktop.initializers;

import ru.ntcap.core.recipes.Histogram;
import ru.ntcap.desktop.events.Event;

import java.util.Random;

public class HistogramRandomPlaneInitializer implements PlaneInitializer {
    @Override
    public void initialize(Event event) {
        Histogram histogram = (Histogram) event.getParam("cellularAutomaton");
        Random random = new Random();
        float probability = (Float)event.getParam("probability");
        histogram.initPlane(0, i -> {
            if (random.nextFloat() * 100.0f <= probability) {
                return 1;
            }
            return 0;
        });

        histogram.initPlane(1, (x, y) -> {
            if (y != histogram.getPlaneHeight() - 1)
                return 0;
            return 1;
        });
    }
}
