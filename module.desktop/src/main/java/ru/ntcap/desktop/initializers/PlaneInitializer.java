package ru.ntcap.desktop.initializers;

import ru.ntcap.desktop.events.Event;

public interface PlaneInitializer {
    void initialize(Event event);
}
