package ru.ntcap.desktop.initializers;

import ru.ntcap.desktop.events.Event;
import ru.ntcap.desktop.events.EventHandler;

import java.util.Map;

public class InitPlaneInteractor implements EventHandler {
    @Override
    public void handleEvent(Event event) {
        if (!event.hasName("init-plane"))
            return;

        String recipeName = (String)event.getParam("recipeName");
        String patternName = (String)event.getParam("patternName");

        INITIALIZERS.get(recipeName).get(patternName).initialize(event);
    }

    private final Map<String, Map<String, PlaneInitializer>> INITIALIZERS = Map.of(
        "life", Map.of(
            "random", new LifeRandomPlaneInitializer()
        ),
        "histogram", Map.of(
            "random", new HistogramRandomPlaneInitializer()
        ),
        "phase-sensible-gas", Map.of(
            "random", new PhaseSensibleGasRandomPlaneInitializer()
        ),
        "cyclic-rank", Map.of(
            "random", new CyclicRankRandomPlaneInitializer()
        )
    );
}
