/*NTCAP Copyright (C) 2021-2022  Georgij Krajnyukov <nrk63@yandex.ru>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package ru.ntcap.desktop;

import ru.ntcap.core.CellularAutomatonView;
import ru.ntcap.core.CellularAutomaton;
import ru.ntcap.desktop.colormap.ColorMap;

import javax.swing.JComponent;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferInt;
import java.util.Objects;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * This {@code JComponent} runs cellular automaton in backing thread and shows the result
 * using specified {@link DisplayRule}. This class is mutable, and it's thread-safety is
 * still being considered.
 *
 * @author Georgij Krajnyukov
 * @version 0.2.0
 */
public class CellularAutomatonLabel extends JComponent {
    /**
     * Creates new instance with specified cellular automaton and display rule.
     * Steps per second has default value.
     * @param source cellular automaton
     * @param displayRule display rule
     *
     * @throws NullPointerException if cellular automaton or display rule is null
     */
    public CellularAutomatonLabel(CellularAutomaton source, DisplayRule displayRule) {
        super();
        setDoubleBuffered(false);
        setEnabled(true);
        setFocusable(true);
        setVisible(true);

        setSource(source);
        setDisplayRule(displayRule);
        setStepsPerSecond(DEFAULT_STEPS_PER_SECOND);
        running = false;

        labelImage = new BufferedImage(source.getPlaneWidth(), source.getPlaneHeight(),
                                        BufferedImage.TYPE_INT_RGB);
        screen = ((DataBufferInt) labelImage.getRaster().getDataBuffer()).getData();
    }

    /**
     * Starts cellular automaton computing cycle.
     */
    public void start() {
        running = true;
        computingExecutorService.execute(computingCycle);
    }

    /**
     * Stops cellular automaton computing cycle.
     */
    public void stop() {
        running = false;
    }

    /**
     * Checks if cellular automaton computing cycle is running.
     * @return {@code true} if computing cycle is running, {@code false} otherwise
     */
    public boolean isRunning() {
        return running;
    }

    /**
     * Sets another cellular automaton to this instance.
     * @param source cellular automaton
     *
     * @throws NullPointerException if cellular automaton is null
     */
    public void setSource(CellularAutomaton source) {
        Objects.requireNonNull(source);
        this.source = source;
        repaint();
    }

    /**
     * Gets this instance display rule.
     * @return display rule
     */
    public DisplayRule getDisplayRule() {
        return displayRule;
    }

    /**
     * Sets another display rule to this instance.
     * @param displayRule display rule
     *
     * @throws NullPointerException if display rule is null
     */
    public void setDisplayRule(DisplayRule displayRule) {
        Objects.requireNonNull(displayRule);
        this.displayRule = displayRule;
        repaint();
    }

    public void setColorMap(ColorMap colorMap) {
        displayRule.setColorMap(colorMap);
        repaint();
    }

    /**
     * Sets cellular automaton steps per second rate.
     * @param stepsPerSecond number of steps per second
     */
    public void setStepsPerSecond(double stepsPerSecond) {
        if (stepsPerSecond <= 0)
            throw new IllegalArgumentException("non-positive steps per second: " + stepsPerSecond);
        this.computingPartTime = 1e9/stepsPerSecond;
    }

    /**
     * Paints this component based on this instance cellular automaton and display rule.
     * @implSpec
     * This implementation uses {@link DisplayRule#setRgbPixels(Dimension, int[], CellularAutomatonView)}
     * to retrieve data from cellular automaton.
     * @param g awt graphic context
     */
    @Override
    protected void paintComponent(Graphics g) {
        Dimension imageSize = new Dimension(labelImage.getWidth(), labelImage.getHeight());
        displayRule.setRgbPixels(imageSize, screen, source);
        g.drawImage(labelImage, 0, 0, null);
    }

    /**
     * cellular automaton, the data source
     */
    private CellularAutomaton source;

    /**
     * display rule
     */
    private DisplayRule displayRule;

    /**
     * int rgb type {@code BufferedImage} on which cellular automaton data is painted
     */
    private final BufferedImage labelImage;

    /**
     * int rgb pixels of {@link #labelImage}
     */
    private final int[] screen;

    /**
     * time period between computing cycle steps measured in nanoseconds
     */
    private double computingPartTime;

    /**
     * flag indicating if cellular automaton computing cycle is running
     */
    private boolean running;

    /**
     * cellular automaton computing cycle function
     */
    private final Runnable computingCycle = () -> {
        long lastUpdateTime = 0;
        while (running) {
            long nowTime = System.nanoTime();
            if (nowTime - lastUpdateTime >= computingPartTime) {
                source.makeStep();
                repaint();
                lastUpdateTime = nowTime;
            }
        }
    };

    /**
     * computing cycle executor
     */
    private final ExecutorService computingExecutorService = Executors.newSingleThreadExecutor();

    /**
     * default number of computing cycle steps per second
     */
    private static final double DEFAULT_STEPS_PER_SECOND = 10.0;
}
